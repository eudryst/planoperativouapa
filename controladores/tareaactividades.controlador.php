<?php

class Controladortareaactividad{

	/*=============================================
	CREAR credit_cardS
	=============================================*/

	static public function ctrCreartarea(){


		if(isset($_POST["add_tarea"])){


			$tabla = "tarea_actividades";

			$datos = array(	'DescTarea' 				=> $_POST['DescTarea'],
							'Idactividades'				=> $_POST["Idactividades"]

						);
				
			$respuesta = Modelotareaactividad::mdlRegistrotarea($tabla, $datos);

			if($respuesta == 'ok'){

				ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' creo la tarea '.$_POST['NomTarea']);

				echo'<script>

				Swal.fire({
					  type: "success",
					  title: "Datos registrado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

								}
							})

				</script>';

			}else{

			echo'<script>

				Swal.fire({
					  type: "error",
					  title: "¡ERROR al registrar los datos!",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
						if (result.value) {

						window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

						}
					})

		  	</script>';

			}

		}

	}


	/*=============================================
	MOSTRAR credit_cardS
	=============================================*/
	static public function ctrMostrarActividad($Idactividades){

		$tabla = "actividad";

		$respuesta = Modelotareaactividad::mdlMostrarActividad($tabla,$Idactividades);

		return $respuesta;
	}

	static public function ctrMostrartarea($Idactividades){

		$tabla = "tarea_actividades";

		$respuesta = Modelotareaactividad::mdlMostrartarea($tabla,$Idactividades);

		return $respuesta;
	}

	/*=============================================
	MOSTRAR Detalle de Persona
	=============================================*/

	static public function ctrEditartarea(){

		if(isset($_POST["edit_tarea"])){

			$tabla = "tarea_actividades";

			$datos = array(	'Idtareaactividades'		=> $_POST["Idtareaactividades"],
							'NomTarea' 					=> $_POST["NomTarea"],
							'DescTarea' 				=> $_POST['DescTarea'],
							'EstadoTarea' 				=> $_POST["EstadoTarea"],
							'Idevidencia'				=> $_POST["Idevidencia"]

						);
				
			$respuesta = Modelotareaactividad::mdlEditartarea($tabla, $datos);

			if($respuesta == 'ok'){

				ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' creo la tarea '.$_POST['NomTarea']);

				echo'<script>

				Swal.fire({
					  type: "success",
					  title: "Datos registrado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

								}
							})

				</script>';

			}else{

			echo'<script>

				Swal.fire({
					  type: "error",
					  title: "¡ERROR al registrar los datos!",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
						if (result.value) {

						window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

						}
					})

		  	</script>';

			}

		}
	}

	/*=============================================
	Eliminar
	=============================================*/

	static public function ctrEliminartarea(){

		if(isset($_POST["eliminar_tarea"])){

			$tabla = "tarea_actividades";

			$datos = array(
							'Idtareaactividades' 		=> $_POST['Idtareaactividades']
						);
				
			$respuesta = Modelotareaactividad::mdlEliminartarea($tabla, $datos);

			if($respuesta == 'ok'){

				ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' eliminio la tarea '.$_POST['Idtareaactividades']);

				echo'<script>

				Swal.fire({
					  type: "success",
					  title: "Datos eliminado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

								}
							})

				</script>';

			}else{

			echo'<script>

				Swal.fire({
					  type: "error",
					  title: "¡ERROR al eliminar los datos!",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
						if (result.value) {

						window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

						}
					})

		  	</script>';

			}

		}
	}


	/*/ ////////////////////////////////////////////////////////
	Evidencias
	////////////////////////////////////////////////////////////*/



	static public function ctrCrearEvidencia(){


		if (isset($_POST['add_tarea2'])) {

			$tabla 		= "tarea_detalle";

			$datos 		= array('Idtareaactividades' 	=> $_POST["IdEvidencia"],
								'accion' 				=> $_POST['DescTarea'],
								'estado' 				=> $_POST['EstadoTarea'],
								'progreso' 				=> $_POST['progreso']);

			$id_evidencia = Modelotareaactividad::mdlRegistrarEvidencia($tabla, $datos);


			$tabla 		= "img_temp";
			$item 		= "id_user";
			$valor		= $_SESSION["id_user"];

			$img_temp 	= Modelotareaactividad::mdlMostrarImgTemp($tabla, $item, $valor);

			if($id_evidencia > 0 AND count($img_temp) > 0){

				$UrlTemp = "vistas/dist/img/evidencias/";

				$UrlNew	 = "vistas/dist/img/evidencias/".$id_evidencia."/";

				@mkdir($UrlNew, 0777, true);


				foreach ($img_temp as $key => $value) {

					$aleatorio  = mt_rand(100,999);

					if (@!copy($UrlTemp.$value['name'], $UrlNew.$aleatorio.'.jpg')) {

					    //echo "Error al copiar ...\n";

					}else{

						$tabla = "evidencia";

						$datos_img = array(	"id_detalle" 	=> $id_evidencia,
					           				"foto"			=> $aleatorio.'.jpg');

						Modelotareaactividad::mdlInsertImg($tabla,$datos_img);

						$tabla 	= "img_temp";

						$valor	= $_SESSION["id_user"];

						Modelotareaactividad::mdlDelTempImg($tabla,'id_user', $valor);

						@unlink($UrlTemp.$value['name']); //elimino el fichero

						sleep(2);
					}

				}


						echo '<script>

								Swal.fire({

									type: "success",
									title: "¡Datos registrado correctamente!",
									showConfirmButton: true,
									confirmButtonText: "Cerrar"

								}).then(function(result){

									if(result.value){

										window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

									}

								});

							</script>';


			}else {

				echo '<script>

					Swal.fire({

						type: "danger",
						title: "¡Error al registrar datos!",
						showConfirmButton: true,
						confirmButtonText: "Cerrar"

					}).then(function(result){

						if(result.value){

							window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

						}

					});


					</script>';
			}

		}
		/*///////////////////////////////////////////
		Carga de imagenes
		///////////////////////////////////////////*/

		if (!empty($_FILES['file'])) {


			$tempFile 	= $_FILES['file']['tmp_name'];       //3
			$nombre 	= $_FILES["file"]["name"];
			
			/*=============================================
			CREAMOS EL DIRECTORIO DONDE VAMOS A GUARDAR LA FOTO DEL USUARIO
			=============================================*/

			$directorio = "vistas/dist/img/evidencias/";

			@mkdir($directorio, 0777, true);

		  	$targetPath = $directorio ;  //4 .dirname( __FILE__ ).$ds.

		  	$aleatorio  = mt_rand(100,999);

		  	$tabla = "img_temp";

			$datos = array(	"id_user" 	=> $_SESSION["id_user"],
						    "name" 		=> $nombre);//.".jpg"

			$respuesta = Modelotareaactividad::mdlImgTemp($tabla, $datos);

		  	$targetPath = $directorio.$nombre;//.".jpg"

		   	$targetFile =  $targetPath;  //5 $_FILES['file']['name']

		    move_uploaded_file($tempFile,$targetFile); //6

		}
	}


	static public function ctrMostrarTareaDetalle($Idactividades){

		$tabla 		= "tarea_detalle";

		$respuesta 	= Modelotareaactividad::mdlMostrarTareaDetalle($tabla,$Idactividades);

		return $respuesta;
	}

	static public function ctrMostrarEvidencia($IdDetalle){

		$tabla 		= "evidencia";

		$respuesta 	= Modelotareaactividad::mdlMostrarEvidencia($tabla,$IdDetalle);

		return $respuesta;
	}


}
?>