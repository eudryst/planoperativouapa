<?php

class ControladorEvidencias{

	/*=============================================
	CREAR credit_cardS
	=============================================*/

	static public function ctrCrearEvidencias(){


		if(isset($_POST["add_Evidencias"])){

			$tabla = "evidencia";

			$datos = array(	'Descevidencia' 		=> $_POST["Descevidencia"],
							'Foto' 					=> $_POST['Foto']
						);
				
			$respuesta = ModeloEvidencias::mdlRegistroEvidencias($tabla, $datos);

			if($respuesta == 'ok'){

				ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' creo la evidencia '.$_POST['Descevidencia']);

				echo'<script>

				Swal.fire({
					  type: "success",
					  title: "Datos registrado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

								}
							})

				</script>';

			}else{

			echo'<script>

				Swal.fire({
					  type: "error",
					  title: "¡ERROR al registrar los datos!",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
						if (result.value) {

						window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

						}
					})

		  	</script>';

			}

		}

	}


	/*=============================================
	MOSTRAR credit_cardS
	=============================================*/

	static public function ctrMostrarEvidencias(){

		$tabla = "evidencia";

		$respuesta = ModeloEvidencias::mdlMostrarEvidencias($tabla);

		return $respuesta;
	}

	/*=============================================
	MOSTRAR Detalle de Persona
	=============================================*/

	static public function ctrEditarEvidencias(){

		if(isset($_POST["edit_Evidencias"])){

			$tabla = "evidencia";

			$datos = array(	'Descevidencia' 		=> $_POST["Descevidencia"],
							'Foto' 		=> $_POST['Foto'],
							'Idevidencia' 		=> $_POST['Idevidencia']
						);
				
			$respuesta = ModeloEvidencias::mdlEditarEvidencias($tabla, $datos);

			if($respuesta == 'ok'){

				ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' creo la evidencia '.$_POST['Descevidencia']);

				echo'<script>

				Swal.fire({
					  type: "success",
					  title: "Datos registrado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

								}
							})

				</script>';

			}else{

			echo'<script>

				Swal.fire({
					  type: "error",
					  title: "¡ERROR al registrar los datos!",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
						if (result.value) {

						window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

						}
					})

		  	</script>';

			}

		}
	}

	/*=============================================
	Eliminar
	=============================================*/

	static public function ctrEliminarEvidencias(){

		if(isset($_POST["eliminar_Evidencias"])){

			$tabla = "evidencia";

			$datos = array(
							'Idevidencia' 		=> $_POST['Idevidencia']
						);
				
			$respuesta = ModeloEvidencias::mdlEliminarEvidencias($tabla, $datos);

			if($respuesta == 'ok'){

				ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' eliminio el eje '.$_POST['Idevidencia']);

				echo'<script>

				Swal.fire({
					  type: "success",
					  title: "Datos eliminado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

								}
							})

				</script>';

			}else{

			echo'<script>

				Swal.fire({
					  type: "error",
					  title: "¡ERROR al eliminar los datos!",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
						if (result.value) {

						window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

						}
					})

		  	</script>';

			}

		}
	}
}
?>