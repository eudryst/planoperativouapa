<?php

class ControladorEjes{

	/*=============================================
	CREAR credit_cardS
	=============================================*/

	static public function ctrCrearEjes(){


		if(isset($_POST["add_ejes"])){

			$tabla = "ejes";

			$datos = array(	'NomEjes' 		=> $_POST["NomEjes"],
							'DescEjes' 		=> $_POST['DescEjes']
						);
				
			$respuesta = ModeloEjes::mdlRegistroEjes($tabla, $datos);

			if($respuesta == 'ok'){

				ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' creo el eje '.$_POST['NomEjes']);

				echo'<script>

				Swal.fire({
					  type: "success",
					  title: "Datos registrado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

								}
							})

				</script>';

			}else{

			echo'<script>

				Swal.fire({
					  type: "error",
					  title: "¡ERROR al registrar los datos!",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
						if (result.value) {

						window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

						}
					})

		  	</script>';

			}

		}

	}


	/*=============================================
	MOSTRAR credit_cardS
	=============================================*/

	static public function ctrMostrarEjes(){

		$tabla = "ejes";

		$respuesta = ModeloEjes::mdlMostrarEjes($tabla);

		return $respuesta;
	}

	/*=============================================
	MOSTRAR Detalle de Persona
	=============================================*/

	static public function ctrEditarEjes(){

		if(isset($_POST["edit_ejes"])){

			$tabla = "ejes";

			$datos = array(	'NomEjes' 		=> $_POST["NomEjes"],
							'DescEjes' 		=> $_POST['DescEjes'],
							'IdEjes' 		=> $_POST['IdEjes']
						);
				
			$respuesta = ModeloEjes::mdlEditarEjes($tabla, $datos);

			if($respuesta == 'ok'){

				ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' creo el eje '.$_POST['NomEjes']);

				echo'<script>

				Swal.fire({
					  type: "success",
					  title: "Datos registrado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

								}
							})

				</script>';

			}else{

			echo'<script>

				Swal.fire({
					  type: "error",
					  title: "¡ERROR al registrar los datos!",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
						if (result.value) {

						window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

						}
					})

		  	</script>';

			}

		}
	}

	/*=============================================
	Eliminar
	=============================================*/

	static public function ctrEliminarEjes(){

		if(isset($_POST["eliminar_ejes"])){

			$tabla = "ejes";

			$datos = array(
							'IdEjes' 		=> $_POST['IdEjes']
						);
				
			$respuesta = ModeloEjes::mdlEliminarEjes($tabla, $datos);

			if($respuesta == 'ok'){

				ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' eliminio el eje '.$_POST['IdEjes']);

				echo'<script>

				Swal.fire({
					  type: "success",
					  title: "Datos eliminado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

								}
							})

				</script>';

			}else{

			echo'<script>

				Swal.fire({
					  type: "error",
					  title: "¡ERROR al eliminar los datos!",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
						if (result.value) {

						window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

						}
					})

		  	</script>';

			}

		}
	}
}
?>