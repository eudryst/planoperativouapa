<?php

class Controladorlineasactuacion{

	/*=============================================
	CREAR credit_cardS
	=============================================*/

	static public function ctrCrearlineasactuacion(){


		if(isset($_POST["add_lineasactuacion"])){

			$tabla = "lineas_actuacion";

			$datos = array(	'Nomlinactuacion' 		=> $_POST["Nomlinactuacion"],
							'DescLinactuacion' 		=> $_POST['DescLinactuacion'],
							'Idobjtactico' 			=> $_POST['Idlineasactuacion']
						);
				print_r($datos);
			$respuesta = Modelolineasactuacion::mdlRegistrolineasactuacion($tabla, $datos);

			if($respuesta == 'ok'){

				ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' creo la linea '.$_POST['Nomlinactuacion']);

				echo'<script>

				Swal.fire({
					  type: "success",
					  title: "Datos registrado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

								}
							})

				</script>';

			}else{

			echo'<script>

				Swal.fire({
					  type: "error",
					  title: "¡ERROR al registrar los datos!",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
						if (result.value) {

						window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

						}
					})

		  	</script>';

			}

		}

	}


	/*=============================================
	MOSTRAR credit_cardS
	=============================================*/

	static public function ctrMostrarlineasactuacion($Idobjtacticos){

		$tabla = "lineas_actuacion";

		$respuesta = Modelolineasactuacion::mdlMostrarlineasactuacion($tabla, $Idobjtacticos);

		return $respuesta;
	}

	static public function ctrMostrarlineasactuacionDashboard(){
		

		$respuesta = Modelolineasactuacion::mdlMostrarlineasactuacionDashboard();

		return $respuesta;
	}


	/*=============================================
	MOSTRAR Detalle de Persona
	=============================================*/

	static public function ctrEditarlineasactuacion(){

		if(isset($_POST["edit_lineas_actuacion"])){

			$tabla = "lineas_actuacion";

			$datos = array(	'Nomlinactuacion' 		=> $_POST["Nomlinactuacion"],
							'DescLinactuacion' 		=> $_POST['DescLinactuacion'],
							'Idlinactuacion' 		=> $_POST['Idlinactuacion']
						);
				
			$respuesta = Modelolineasactuacion::mdlEditarlineasactuacion($tabla, $datos);

			if($respuesta == 'ok'){

				ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' creo la linea '.$_POST['Nomlinactuacion']);

				echo'<script>

				Swal.fire({
					  type: "success",
					  title: "Datos registrado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

								}
							})

				</script>';

			}else{

			echo'<script>

				Swal.fire({
					  type: "error",
					  title: "¡ERROR al registrar los datos!",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
						if (result.value) {

						window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

						}
					})

		  	</script>';

			}

		}
	}

	/*=============================================
	Eliminar
	=============================================*/

	static public function ctrEliminarlineasactuacion(){

		if(isset($_POST["eliminar_lineas_actuacion"])){

			$tabla = "lineas_actuacion";

			$datos = array(
							'Idlinactuacion' 		=> $_POST['Idlinactuacion']
						);
				
			$respuesta = Modelolineasactuacion::mdlEliminarlineasactuacion($tabla, $datos);

			if($respuesta == 'ok'){

				ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' eliminio la linea '.$_POST['Idlinactuacion']);

				echo'<script>

				Swal.fire({
					  type: "success",
					  title: "Datos eliminado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

								}
							})

				</script>';

			}else{

			echo'<script>

				Swal.fire({
					  type: "error",
					  title: "¡ERROR al eliminar los datos!",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
						if (result.value) {

						window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

						}
					})

		  	</script>';

			}

		}
	}
}
?>