<?php

class Controladorobjetivostacticos{

	/*=============================================
	CREAR credit_cardS
	=============================================*/

	static public function ctrCrearobjetivostacticos(){


		if(isset($_POST["add_objetivostacticos"])){

			$tabla = "obj_tactico";

			$datos = array(	'Nomobjtactico' 		=> $_POST["Nomobjtactico"],
							'descobjtactico' 		=> $_POST['descobjtactico'],
							'Idobjestratejico' 		=> $_POST['Idobjestratejicos']
						);

			$respuesta = Modeloobjetivostacticos::mdlRegistroobjetivostacticos($tabla, $datos);

			if($respuesta == 'ok'){

				ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' creo objetivo '.$_POST['Nomobjtactico']);

				echo'<script>

				Swal.fire({
					  type: "success",
					  title: "Datos registrado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

								}
							})

				</script>';

			}else{

			echo'<script>

				Swal.fire({
					  type: "error",
					  title: "¡ERROR al registrar los datos!",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
						if (result.value) {

						window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

						}
					})

		  	</script>';

			}

		}

	}


	/*=============================================
	MOSTRAR credit_cardS
	=============================================*/

	static public function ctrMostrarobjetivostacticos($Idobjestratejicos){

		$tabla = "obj_tactico";

		$respuesta = Modeloobjetivostacticos::mdlMostrarobjetivostacticos($tabla,$Idobjestratejicos);

		return $respuesta;
	}

	static public function ctrMostrarobjetivostacticosDashboard(){

		
		$respuesta = Modeloobjetivostacticos::mdlMostrarobjetivostacticosDashboard();

		return $respuesta;
	}

	/*=============================================
	MOSTRAR Detalle de Persona
	=============================================*/

	static public function ctrEditarobjetivostacticos(){

		if(isset($_POST["edit_obj_tactico"])){

			$tabla = "obj_tactico";

			$datos = array(	'Nomobjtactico' 		=> $_POST["Nomobjtactico"],
							'descobjtactico' 		=> $_POST['descobjtactico'],
							'Idobjtactico' 			=> $_POST['Idobjtactico']
						);
				
			$respuesta = Modeloobjetivostacticos::mdlEditarobjetivostacticos($tabla, $datos);

			if($respuesta == 'ok'){

				ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' creo objetivo '.$_POST['Nomobjtactico']);

				echo'<script>

				Swal.fire({
					  type: "success",
					  title: "Datos registrado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

								}
							})

				</script>';

			}else{

			echo'<script>

				Swal.fire({
					  type: "error",
					  title: "¡ERROR al registrar los datos!",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
						if (result.value) {

						window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

						}
					})

		  	</script>';

			}

		}
	}

	/*=============================================
	Eliminar
	=============================================*/

	static public function ctrEliminarobjetivostacticos(){

		if(isset($_POST["eliminar_obj_tactico"])){

			$tabla = "obj_tactico";

			$datos = array(
							'Idobjtactico' 		=> $_POST['Idobjtactico']
						);
				
			$respuesta = Modeloobjetivostacticos::mdlEliminarobjetivostacticos($tabla, $datos);

			if($respuesta == 'ok'){

				ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' eliminio objetivo '.$_POST['Idobjtactico']);

				echo'<script>

				Swal.fire({
					  type: "success",
					  title: "Datos eliminado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

								}
							})

				</script>';

			}else{

			echo'<script>

				Swal.fire({
					  type: "error",
					  title: "¡ERROR al eliminar los datos!",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
						if (result.value) {

						window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

						}
					})

		  	</script>';

			}

		}
	}
}
?>