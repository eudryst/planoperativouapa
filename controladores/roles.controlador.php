<?php

class ControladorPermiso{

	/*=============================================
	CREAR Permiso
	=============================================*/

	static public function ctrCrearRoles(){

	if(!empty($_POST['AddRoles']))
	{
			if (!empty($_SESSION["id_user"])) {

				$tabla 			= "roles";

				$datos 			= array("nombre"	=> $_POST["nombre"]);

				$IdRol	 		= ModeloPermiso::mdlRegistroRoles($tabla, $datos);

				foreach ($_POST['permiso'] as $key => $value) {

					$tabla 			= "roles_permiso";

					$datos 			= array("id_roles"		=> $IdRol,
											"id_permiso"	=> $value);

					$respuesta = ModeloPermiso::mdlRegistroRolesPermisos($tabla, $datos);

				}


					if($respuesta == "ok"){

						ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' creo el rol '.$_POST["nombre"]);

							echo '<script>

							Swal.fire({

								type: "success",
								title: "Datos registrado correctamente.!",
								showConfirmButton: true,
								confirmButtonText: "Cerrar"

							}).then(function(result){

								if(result.value){

									window.location = "roles";

								}

							});


							</script>';

					} else {

						echo '<script>

							Swal.fire({

								type: "error",
								title: "¡Error! Datos no registrado!",
								showConfirmButton: true,
								confirmButtonText: "Cerrar"

							}).then(function(result){

								if(result.value){

									window.location = "rolesadd";

								}

							});


							</script>';
					}

			}
		}

	}

	/*=============================================
	Update Role
	=============================================*/

	static public function ctrUpdateRoles(){

	if(!empty($_POST['UpdateRoles']))
	{
			if (!empty($_SESSION["id_user"])) {

				$tabla 			= "roles";

				$datos 			= array("nombre"	=> $_POST["nombre"],
										"id_roles"	=> $_POST["id_roles"]);

				$IdRol	 		= ModeloPermiso::mdlUpdateRoles($tabla, $datos);

				$tabla 			= "roles_permiso";

				$respuesta 		= ModeloPermiso::mdlBorrarPermiso($tabla, 'id_roles', $_POST["id_roles"]);

				if (!empty($_POST['permiso'])) {

					foreach ($_POST['permiso'] as $key => $value) {

						$datos 			= array("id_roles"		=> $_POST["id_roles"],
												"id_permiso"	=> $value);

						$respuesta = ModeloPermiso::mdlRegistroRolesPermisos($tabla, $datos);

					}

				}


					if($respuesta == "ok"){

							ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' realizo cambio en el rol '.$_POST["nombre"]);

							echo '<script>

							Swal.fire({

								type: "success",
								title: "Datos registrado correctamente.!",
								showConfirmButton: true,
								confirmButtonText: "Cerrar"

							}).then(function(result){

								if(result.value){

									window.location = "'.$_SERVER["REQUEST_URI"].'";

								}

							});


							</script>';

					} else {

						echo '<script>

							Swal.fire({

								type: "error",
								title: "¡Error! Datos no registrado!",
								showConfirmButton: true,
								confirmButtonText: "Cerrar"

							}).then(function(result){

								if(result.value){

									window.location = "'.$_SERVER["REQUEST_URI"].'";

								}

							});


							</script>';
					}

			}
		}

	}
	/*=============================================
	MOSTRAR Permiso
	=============================================*/

	static public function ctrMostrarPermisoUser($valor){

		$respuesta = ModeloPermiso::mdlMostrarPermisoUser($valor);

		return $respuesta;
	}


	/*=============================================
	MOSTRAR Roles
	=============================================*/

	static public function ctrMostrarRoles(){

		$respuesta = ModeloPermiso::mdlMostrarRoles();

		return $respuesta;
	}

	/*=============================================
	MOSTRAR Permiso
	=============================================*/

	static public function ctrMostrarPermiso($value){

		$respuesta = ModeloPermiso::mdlMostrarPermiso($value);

		return $respuesta;
	}

}