<?php

class ControladorPersona{

	/*=============================================
	CREAR credit_cardS
	=============================================*/

	static public function ctrCrearPersona(){

		if(isset($_POST["add_persona"])){

			if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ -]+$/', $_POST["PerCedula"])){

				$tabla = "persona";

				$datos = array(	'IdEmpresa' 			=> $_SESSION["id_empresa"],
								'PerNombre' 			=> $_POST['PerNombre'],
								'PerApellido' 			=> $_POST['PerApellido'],
								'apodo' 				=> $_POST['apodo'],
								'PerFecha_nacimiento' 	=> date('Y-m-d',strtotime($_POST['PerFecha_nacimiento'])),
								'PerSexo' 				=> $_POST['PerSexo'],
								'PerTelefono' 			=> $_POST['PerTelefono'],
								'PerCedula' 			=> $_POST['PerCedula'],
								'PerEmail' 				=> $_POST['PerEmail'],
								'direccion' 			=> $_POST['PerDireccion'],
								'ocupacion' 			=> $_POST['ocupacion'],
								'tel_trabajo' 			=> $_POST['TelTrabajo'],
								'documento' 			=> $_POST['TypeDocument'],
								'observacion' 			=> $_POST['observacion']);

				$respuesta = ModeloPersona::mdlRegistroPersona($tabla, $datos);

				if($respuesta > 0){

					ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' creo el cliente '.$_POST['PerNombre'].' '.$_POST['PerApellido'].' Cédula '.$_POST['PerCedula']);

					echo'<script>

					Swal.fire({
						  type: "success",
						  title: "Datos registrado correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "clientes";

									}
								})

					</script>';

				}else{

				echo'<script>

					Swal.fire({
						  type: "error",
						  title: "¡ERROR al registrar los datos!",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
							if (result.value) {

							window.location = "clientes";

							}
						})

			  	</script>';

			}


			}else{

				echo'<script>

					Swal.fire({
						  type: "error",
						  title: "¡ERROR al registrar los datos!",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
							if (result.value) {

							window.location = "clientes";

							}
						})

			  	</script>';

			}

		}

	}

	/*=============================================
	Cargar documentos
	=============================================*/

	static public function ctrCargarDocumentos(){

		if(isset($_POST["add_Documentos"])){

				$tabla = "img_temp";

				$Documentos_temp = ModeloPersona::mdlMostrarDocumentos($tabla, 'id_persona', $_SESSION["id_persona"]);

				if(count($Documentos_temp) > 0){

					$UrlTemp = "vistas/img/documentos/temp/";

					$UrlNew	 = "vistas/img/documentos/".$_SESSION["id_persona"]."/";

					if (!file_exists($UrlNew)) {

						mkdir($UrlNew, 0777, true);

					}


					foreach ($Documentos_temp as $key => $value) {

						$aleatorio  = mt_rand(100,999);

						//obtener extensión de un arcivo
						$info 		= new SplFileInfo($value['name']);
						$extension 	= strtolower ($info->getExtension());

		  				$nombre 	= $aleatorio.'.'.$extension;


						if (@!copy($UrlTemp.$value['name'], $UrlNew.$nombre)) {

						    //echo "Error al copiar ...\n";

						}else{

							$tabla = "documento";

							$datos = array(	'id_persona' 			=> $value["id_persona"],
											'nombre_documento' 		=> $nombre,
											'descripcion' 			=> $_POST['imgDescripcion']);

							$respuesta = ModeloPersona::mdlAddDocumentos($tabla, $datos);

							$tabla 	= "img_temp";

							$valor	= $_SESSION["id_persona"];

							ModeloPersona::mdlDelTempDocumentos($tabla,'id_persona', $valor);

							@unlink($UrlTemp.$value['name']); //elimino el fichero

							sleep(1);
						}
					}

					if($respuesta == "ok"){


						ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' agrego documento al cliente # '.$value["id_persona"]);

						echo'<script>

						Swal.fire({
							  type: "success",
							  title: "Datos registrado correctamente",
							  showConfirmButton: true,
							  confirmButtonText: "Cerrar"
							  }).then(function(result){
										if (result.value) {

										window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

										}
									})

						</script>';

					}
				}

		}

		/*///////////////////////////////////////////
		Carga de imagenes
		///////////////////////////////////////////*/

		if (!empty($_FILES['file'])) {

			$tempFile 	= $_FILES['file']['tmp_name'];       //3
			$nombre 	= $_FILES["file"]["name"];

			/*=============================================
			CREAMOS EL DIRECTORIO DONDE VAMOS A GUARDAR LA FOTO DEL USUARIO
			=============================================*/

			$directorio = "vistas/img/documentos/temp/";

			@mkdir($directorio, 0777, true);

		  	$targetPath = $directorio ;  //4 .dirname( __FILE__ ).$ds.

		  	$aleatorio  = mt_rand(100,999);

		  	$tabla = "img_temp";

			$datos = array(	"id_persona" 	=> $_SESSION["id_persona"],
						    "name" 			=> $nombre);//.".jpg"

			$respuesta = ModeloPersona::mdlDocumentosTemp($tabla, $datos);

		  	$targetPath = $directorio.$nombre;//.".jpg"

		   	$targetFile =  $targetPath;  //5 $_FILES['file']['name']

		    move_uploaded_file($tempFile,$targetFile); //6

		}

	}

	/*=============================================
	MOSTRAR credit_cardS
	=============================================*/

	static public function ctrMostrarPersona($item, $valor){

		$tabla = "persona";

		$respuesta = ModeloPersona::mdlMostrarPersona($tabla, $item, $valor,$_SESSION["id_empresa"]);

		return $respuesta;
	}

	/*=============================================
	MOSTRAR Detalle de Persona
	=============================================*/

	static public function ctrMostrarDetallePersona($item, $valor){

		$tabla = "persona";

		$respuesta = ModeloPersona::mdlMostrarDetallePersona($tabla, $item, $valor,$_SESSION["id_empresa"]);

		return $respuesta;
	}

	/*=============================================
	EDITAR credit_card
	=============================================*/

	static public function ctrEditarPersona(){

		if(isset($_POST["Edit_persona"])){

			if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ-]+$/', $_POST["PerCedula"])){

				$tabla = "persona";

				$datos = array(	'IdEmpresa' 			=> $_SESSION["id_empresa"],
								'id_persona' 			=> $_POST["id_persona"],
								'PerNombre' 			=> $_POST['PerNombre'],
								'PerApellido' 			=> $_POST['PerApellido'],
								'apodo' 				=> $_POST['apodo'],
								'PerFecha_nacimiento' 	=> date('Y/m/d',strtotime($_POST['PerFecha_nacimiento'])),
								'PerSexo' 				=> $_POST['PerSexo'],
								'PerTelefono' 			=> $_POST['PerTelefono'],
								'PerCedula' 			=> $_POST['PerCedula'],
								'PerEmail' 				=> $_POST['PerEmail'],
								'PerDireccion' 			=> $_POST['PerDireccion'],
								'ocupacion' 			=> $_POST['ocupacion'],
								'tel_trabajo' 			=> $_POST['TelTrabajo'],
								'documento' 			=> $_POST['TypeDocument'],
								'observacion' 			=> $_POST['observacion']);

				$respuesta = ModeloPersona::mdlEditarPersona($tabla, $datos);

				if($respuesta == "ok"){

					ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' actualizo datos de '.$_POST['PerNombre'].' '.$_POST['PerApellido'].' Cédula '.$_POST['PerCedula']);

					echo'<script>

					Swal.fire({
						  type: "success",
						  title: "Datos registrado correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

									}
								})

					</script>';

				}


			}else{

				echo'<script>

					Swal.fire({
						  type: "error",
						  title: "¡ERROR al registrar los datos!",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
							if (result.value) {

							window.location = "clientes";

							}
						})

			  	</script>';

			}

		}


		//////////////////////////////////////
		//Actualizar imagen de perfil
		//////////////////////////////////////
		if(isset($_FILES['uploadedFile']) && $_FILES['uploadedFile']['error'] === UPLOAD_ERR_OK){

			// get details of the uploaded file
			$fileTmpPath 	= $_FILES['uploadedFile']['tmp_name'];
			$fileName 		= $_FILES['uploadedFile']['name'];
			$fileSize 		= $_FILES['uploadedFile']['size'];
			$fileType 		= $_FILES['uploadedFile']['type'];
			$fileNameCmps 	= explode(".", $fileName);
			$fileExtension 	= strtolower(end($fileNameCmps));

			$newFileName 	= 'logo.' . $fileExtension;

			$allowedfileExtensions = array('jpg', 'jpeg', 'gif', 'png','PNG','JPG','GIF');

			if (in_array($fileExtension, $allowedfileExtensions)) {

				// directory in which the uploaded file will be moved
				$uploadFileDir	 = "vistas/img/personas/".$_SESSION["id_empresa"].'/'.$_POST['IdCliente']."/";

				if (!file_exists($uploadFileDir)) {

					@mkdir($uploadFileDir, 0777, true);
				}

				$dest_path = $uploadFileDir . $newFileName;

				if(move_uploaded_file($fileTmpPath, $dest_path))
				{

					$tabla = "persona";

					$datos = array("imagen"		=>$newFileName,
								   "id_persona"	=>$_POST['IdCliente'],
								);

					$respuesta = ModeloPersona::mdlEditarImagen($tabla, $datos);

					if($respuesta == "ok"){

						ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' actualizo el perfil del cliente '.$_POST['IdCliente']);

						echo'<script>

						Swal.fire({
							  type: "success",
							  title: "Datos actualizado correctamente",
							  showConfirmButton: true,
							  confirmButtonText: "Cerrar"
							  }).then(function(result){
										if (result.value) {

										window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

										}
									})

						</script>';

					}


				}

			}



		}


	}

	/*=============================================
	BORRAR credit_card
	=============================================*/

	static public function ctrBorrarPersona(){

		if(isset($_POST["del_Cliente"])){

			$tabla = "persona";

			$datos = $_POST["id_persona"];

			$respuesta = ModeloPersona::mdlBorrarPersona($tabla, $datos);

			if($respuesta == "ok"){

				ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' elimino a la persona ID: '.$_POST["id_persona"].' Nombre: '.$_POST["nombre"]);

				echo'<script>

					Swal.fire({
						  type: "success",
						  title: "Registro eliminado correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "clientes";

									}
								})

					</script>';
			}else{

				echo'<script>

					Swal.fire({
						  type: "error",
						  title: "¡Error! Registro no eliminado",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

									}
								})

					</script>';

			}
		}

	}


	/*=============================================
	MOSTRAR Documentos
	=============================================*/

	static public function ctrMostrarDocumentos($item, $valor){

		$tabla = "documento";

		$respuesta = ModeloPersona::mdlMostrarDocumentos($tabla, $item, $valor);

		return $respuesta;
	}

	/*=============================================
	ELIMINAR IMG TEMPORAR
	=============================================*/

	static public function ctrEliminarImgTemp($nombre){


				$tabla 		= "img_temp";

				$item 		= 'name';

				$datos 		= $nombre;

				$respuesta 	= ModeloPersona::mdlDelTempDocumentos($tabla, $item, $datos);

				if ($respuesta == 'ok') {

					@unlink("../vistas/img/documentos/temp/".$nombre);

					return $respuesta;

				}else{

					return $respuesta;

				}

	}

	/*=============================================
	ELIMINAR IMG Editar Blog
	=============================================*/

	static public function ctrEliminarDocumento($id){

				$respuesta  	= ControladorPersona::ctrMostrarDocumentos('id_documento', $id);

				$img 			= $respuesta[0]['nombre_documento'];

				$id_persona		= $respuesta[0]['id_persona'];

				$tabla 			= "documento";

				$item 			= 'id_documento';

				$datos 			= $id;

				$respuesta 		= ModeloPersona::mdlDelTempDocumentos($tabla,$item, $datos);

				if ($respuesta == 'ok') {

					@unlink("../vistas/img/documentos/".$id_persona."/".$img);

					return $respuesta;

				}else{

					return $respuesta;

				}


	}

}