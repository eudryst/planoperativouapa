<?php



class ControladorDirectores{

	/*=============================================
	CREAR credit_cardS
	=============================================*/

	static public function ctrCrearDirectores(){

		if(isset($_POST["add_director"])){

			if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ -]+$/', $_POST["NomDirector"])){

				$tabla = "directores";

				$datos = array(	'IdDirector' 			=> $_SESSION["IdDirector"],
								'NomDirector' 			=> $_POST['NomDirector'],
								'Idusuario' 			=> $_POST['Idusuario'],
								'Correo' 				=> $_POST['Correo'],
								'NomDepartamento' 		=> $_POST['NomDepartamento'],
								'Estado' 				=> $_POST['Estado']
							);
								
				$respuesta = ModeloDirectores::mdlRegistroDirectores($tabla, $datos);

				if($respuesta > 0){

					ControladorBitacora::ctrCrearBitacora('El director '.$_SESSION["usuario"].' creo el ususario '.$_POST['perNomDirector'].' '.$_POST['perCorreo'].' NomDepartamento '.$_POST['NomDepartamento']);

					echo'<script>

					Swal.fire({
						  type: "success",
						  title: "Datos registrado correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

									}
								})

					</script>';

				}else{

				echo'<script>

					Swal.fire({
						  type: "error",
						  title: "¡ERROR al registrar los datos!",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
							if (result.value) {

							window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

							}
						})

			  	</script>';

			}


			}else{

				echo'<script>

					Swal.fire({
						  type: "error",
						  title: "¡ERROR al registrar los datos!",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
							if (result.value) {

							window.location = "directores";

							}
						})

			  	</script>';

			}

		}

	}

/*=============================================
	MOSTRAR credit_cardS
	=============================================*/

	static public function ctrMostrarDirectores(){

		$tabla = "directores";

		$respuesta = ModeloDirectores::mdlMostrarDirectores($tabla);

		return $respuesta;
	}

	/*=============================================
	MOSTRAR Detalle de DIRECTORES
	=============================================*/

	static public function ctrEditarDirectores(){

		if(isset($_POST["edit_directores"])){

			$tabla = "directores";

			$datos = array(	'IdDirector' 		=> $_POST["IdDirector"],
							'NomDirector' 		=> $_POST['NomDirector'],
							'Idusuario' 		=> $_POST['Idusuario'],
							'Correo' 			=> $_POST["Correo"],
							'NomDepartamento' 	=> $_POST['NomDepartamento'],
							'Estado' 			=> $_POST['Estado']
						);
				
			$respuesta = ModeloDirectores::mdlEditarDirectores($tabla, $datos);

			if($respuesta == 'ok'){

				ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' creo el director '.$_POST['NomDirector']);

				echo'<script>

				Swal.fire({
					  type: "success",
					  title: "Datos registrado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

								}
							})

				</script>';

			}else{

			echo'<script>

				Swal.fire({
					  type: "error",
					  title: "¡ERROR al registrar los datos!",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
						if (result.value) {

						window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

						}
					})

		  	</script>';

			}

		}
	}

	/*=============================================
	Eliminar
	=============================================*/

	static public function ctrEliminarDirector(){

		if(isset($_POST["eliminar_director"])){

			$tabla = "directores";

			$datos = array(
							'IdDirector' 		=> $_POST['IdDirector']
						);
				
			$respuesta = ModeloDirectores::mdlEliminarDirectores($tabla, $datos);

			if($respuesta == 'ok'){

				ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' eliminio el director '.$_POST['IdDirector']);

				echo'<script>

				Swal.fire({
					  type: "success",
					  title: "Datos eliminado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

								}
							})

				</script>';

			}else{

			echo'<script>

				Swal.fire({
					  type: "error",
					  title: "¡ERROR al eliminar los datos!",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
						if (result.value) {

						window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

						}
					})

		  	</script>';

			}

		}
	}
}
?>