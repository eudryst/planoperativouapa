<?php

class ControladorMetas{

	/*=============================================
	CREAR credit_cardS
	=============================================*/

	static public function ctrCrearMetas(){


		if(isset($_POST["add_metas"])){

			$tabla = "metas";

			$datos = array(	'Nommeta' 			=> $_POST["Nommeta"],
							'Descmetas' 		=> $_POST['Descmetas'],
							'Idlinactuacion' 	=> $_POST['Idlinasctuacion']
						);
				
			$respuesta = ModeloMetas::mdlRegistroMetas($tabla, $datos);

			if($respuesta == 'ok'){

				ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' creo la meta '.$_POST['Nommeta']);

				echo'<script>

				Swal.fire({
					  type: "success",
					  title: "Datos registrado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

								}
							})

				</script>';

			}else{

			echo'<script>

				Swal.fire({
					  type: "error",
					  title: "¡ERROR al registrar los datos!",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
						if (result.value) {

						window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

						}
					})

		  	</script>';

			}

		}

	}


	/*=============================================
	MOSTRAR credit_cardS
	=============================================*/

	static public function ctrMostrarMetas($Idlinasctuacion){

		$tabla = "metas";

		$respuesta = ModeloMetas::mdlMostrarMetas($tabla, $Idlinasctuacion);

		return $respuesta;
	}
	
	static public function ctrMostrarMetasDashboard(){

		
		$respuesta = ModeloMetas::mdlMostrarMetasDashboard();

		return $respuesta;
	}

	/*=============================================
	MOSTRAR Detalle de Persona
	=============================================*/

	static public function ctrEditarMetas(){

		if(isset($_POST["edit_metas"])){

			$tabla = "metas";

			$datos = array(	'Nommeta' 		=> $_POST["Nommeta"],
							'Descmetas' 	=> $_POST['Descmetas'],
							'Idmeta' 		=> $_POST['Idmeta']
						);
				
			$respuesta = ModeloMetas::mdlEditarMetas($tabla, $datos);

			if($respuesta == 'ok'){

				ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' creo la meta '.$_POST['Nommeta']);

				echo'<script>

				Swal.fire({
					  type: "success",
					  title: "Datos registrado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

								}
							})

				</script>';

			}else{

			echo'<script>

				Swal.fire({
					  type: "error",
					  title: "¡ERROR al registrar los datos!",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
						if (result.value) {

						window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

						}
					})

		  	</script>';

			}

		}
	}

	/*=============================================
	Eliminar
	=============================================*/

	static public function ctrEliminarMetas(){

		if(isset($_POST["eliminar_metas"])){

			$tabla = "metas";

			$datos = array(
							'Idmeta' 		=> $_POST['Idmeta']
						);
				
			$respuesta = ModeloMetas::mdlEliminarMetas($tabla, $datos);

			if($respuesta == 'ok'){

				ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' eliminio la meta '.$_POST['Idmeta']);

				echo'<script>

				Swal.fire({
					  type: "success",
					  title: "Datos eliminado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

								}
							})

				</script>';

			}else{

			echo'<script>

				Swal.fire({
					  type: "error",
					  title: "¡ERROR al eliminar los datos!",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
						if (result.value) {

						window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

						}
					})

		  	</script>';

			}

		}
	}
}
?>