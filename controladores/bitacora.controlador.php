<?php

class ControladorBitacora{

	/*=============================================
	Registro de Bitacora
	=============================================*/

	static public function ctrCrearBitacora($accion){

				$tabla = "bitacora";

				$datos = array(	'id_usuario' 	=> $_SESSION["id_user"],
								'accion' 		=> $accion);

				$respuesta = ModeloBitacora::mdlRegistroBitacora($tabla, $datos);




	}

	/*=============================================
	MOSTRAR Bitacora
	=============================================*/

	static public function ctrMostrarBitacora($valor){

		$tabla 		= "bitacora";

		$respuesta 	= ModeloBitacora::mdlMostrarBitacora($tabla, $valor);

		return $respuesta;
	}
}
?>