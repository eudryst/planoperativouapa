<?php

require_once "../controladores/carrito.controlador.php";
require_once "../modelos/carrito.modelo.php";

class AjaxCarrito{

	/*=============================================
	ACTIVAR USUARIO
	=============================================*/

	public $id_carrito;
	public $qty;


	public function ajaxQtyCarrito(){

		$tabla = "carrito";

		$item1 = "id";
		$valor1 = $this->id_carrito;

		$item2 = "cantidad";
		$valor2 = $this->qty;

		$respuesta = ModeloCarrito::mdlEditarQtyCarrito($tabla, $item1, $valor1, $item2, $valor2);

		echo json_encode($respuesta);
	}

}


/*=============================================
ACTIVAR USUARIO
=============================================*/

if(isset($_POST["qty"])){

	$QtyCarrito = new AjaxCarrito();
	$QtyCarrito -> id_carrito = $_POST["id_carrito"];
	$QtyCarrito -> qty = $_POST["qty"];
	$QtyCarrito -> ajaxQtyCarrito();

}