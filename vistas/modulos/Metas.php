<?php 

  $valor    = $rutas[0];

  $Idmetas    = Funtion::after("-",$valor);

  $Metas = ControladorMetas::ctrMostrarMetas($Idmetas);

 ?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Metas</h1>
          </div> 
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
             <!--  <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Starter Page</li> -->
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-12">
            <div class="card shadow mb-4">
              <div class="card-header py-3">
               <!--  <h6 class="m-1 font-weight-bold text-primary">Metas</h6> -->
                  <a href="" class="btn btn-outline-primary float-right mb-2" data-toggle="modal" data-target="#AddMetas">
                    <i class="far fa-plus-square"></i>
                  </a>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <!--dentro de este div va mi contenido-->
                  <table class="table" id="TableClientes">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Descripcion</th>
                        <!--  <th>Estado</th>  -->
                        <th>Indicador</th>                       
                        <th>Opciones</th>                 
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($Metas as $key => $value) { ?>
                        <tr>
                          <td><?=$value['Idmeta']?></td>                         
                          <td><a href="Actividades-<?=$value['Idmeta']?>"><?=$value['Nommeta']?></a></td>
                          <td><?=$value['Descmetas']?></td>
                          <!-- <td></td> -->
                          <td>
                             <div class="progress bg-primary">
                                <div class="progress-bar" role="progressbar" aria-valuenow="40" aria-valuenin="0" aria-valuesmax="100" style="width:0%">
                                  60% completad0 (success)
                                </div>          
                              </div>
                          </td>
                          <td>
                            <a href="#" class="far fa-edit" data-toggle="modal" data-target="#EditMetas<?=$value['Idmeta']?>"></a>|
                            <a href="" class="far fa-trash-alt btn-danger" data-toggle="modal" data-target="#EliminarMetas<?=$value['Idmeta']?>"></a>
                          </td>
                        </tr>

                          <div class="modal fade" id="EditMetas<?=$value['Idmeta']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Editar Metas</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                        <form class="user" method="post">
                                          <div class="form-group ">
                                              <label>Nombre</label>
                                              <input type="text" name="Nommeta" value="<?=$value['Nommeta']?>" class="form-control form-control-user" placeholder="Nombre">
                                          </div>
                                          <div class="form-group ">
                                              <label>Descipción</label>
                                              <textarea name="Descmetas" class="form-control form-control-user" placeholder="Nombre"><?=$value['Descmetas']?></textarea>
                                          </div>

                                          <input type="hidden" name="Idmeta" value="<?=$value['Idmeta']?>">

                                          <button type="submit" class="btn btn-primary btn-user btn-block" name="edit_metas">
                                              Registrar Datos
                                          </button>
                                          <hr>
                                        </form>

                                        <?php 
                                          $Crear = new ControladorMetas();
                                          $Crear -> ctrEditarMetas();

                                        ?>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="modal fade" id="EliminarMetas<?=$value['Idmeta']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-md" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Eliminar Metas</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                        <form class="user" method="post">
                                          <h3>ESTA SEGURO QUE DESEA ELIMINAR ESTOS DATOS</h3>
                                          

                                          <input type="hidden" name="Idmeta" value="<?=$value['Idmeta']?>">
                                          
                                          <button type="submit" class="btn btn-danger btn-user btn-block" name="eliminar_metas">
                                              Eliminar Datos
                                          </button>
                                          <hr>
                                        </form>

                                        <?php 
                                          $Crear = new ControladorMetas();
                                          $Crear -> ctrEliminarMetas();

                                        ?>
                                </div>
                              </div>
                            </div>
                          </div>

                      <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
      </div>
    </section>
</div>

  <!-- Modal -->
  <div class="modal fade" id="AddMetas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Agregar Metas</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
                <form class="user" method="post">
                  <div class="form-group ">
                      <label>Nombre</label>
                      <input type="text" name="Nommeta" value="" class="form-control form-control-user" placeholder="Nombre">
                  </div>
                  <div class="form-group ">
                      <label>Descripción</label>
                      <textarea name="Descmetas" class="form-control form-control-user" placeholder="Nombre"></textarea>
                  </div>

                   <input type="hidden" name="Idlinasctuacion" value="<?=$Idmetas?>">

                  <button type="submit" class="btn btn-primary btn-user btn-block" name="add_metas">
                      Registrar Datos
                  </button>
                  <hr>
                </form>

                <?php 
                  $Crear = new ControladorMetas();
                  $Crear -> ctrCrearMetas();

                ?>
        </div>
      </div>
    </div>
  </div>