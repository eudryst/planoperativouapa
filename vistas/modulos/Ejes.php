<?php 

  $ejes = ControladorEjes::ctrMostrarEjes();

 ?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Ejes</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
             <!--  <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Starter Page</li> -->
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-12">
            <div class="card shadow mb-4">
              <div class="card-header py-3">
                <!-- <h6 class="m-1 font-weight-bold text-primary">Ejes</h6> -->
                  <a href="" class="btn btn-outline-primary float-right mb-2" data-toggle="modal" data-target="#AddEjes">
                    <i class="far fa-plus-square"></i>
                  </a>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <!--dentro de este div va mi contenido-->
                  <table class="table" id="TableClientes">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Descripcion</th>
                        <!--  <th>Estado</th> -->
                        <th>Indicador</th>
                        <th>Opciones</th> 
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($ejes as $key => $value) { ?>
                        <tr>
                          <td><?=$value['IdEjes']?></td>
                          <td><a href="ObjetivoEstratejico-<?=$value['IdEjes']?>"><?=$value['NomEjes']?></a></td>                          
                          <td><?=$value['DescEjes']?></td>
                         <!--  <td></td> -->
                          <td>
                             <div class="progress bg-primary">
                                <div class="progress-bar" role="progressbar" aria-valuenow="40" aria-valuenin="0" aria-valuesmax="100" style="width:30%">
                                  60% completad0 (success)
                                </div>          
                              </div>
                          </td>
                          <td>
                            <a href="#" class="far fa-edit" data-toggle="modal" data-target="#EditEjes<?=$value['IdEjes']?>"></a>
                            |
                            <a href="" class="far fa-trash-alt btn-danger" data-toggle="modal" data-target="#EliminarEjes<?=$value['IdEjes']?>"></a>
                          </td>
                        </tr>

                          <div class="modal fade" id="EditEjes<?=$value['IdEjes']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Editar Ejes</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                        <form class="user" method="post">
                                          <div class="form-group ">
                                              <label>Nombre</label>
                                              <input type="text" name="NomEjes" value="<?=$value['NomEjes']?>" class="form-control form-control-user" placeholder="Nombre">
                                          </div>
                                          <div class="form-group ">
                                              <label>Descipción</label>
                                              <textarea name="DescEjes" class="form-control form-control-user" placeholder="Nombre"><?=$value['DescEjes']?></textarea>
                                          </div>

                                          <input type="hidden" name="IdEjes" value="<?=$value['IdEjes']?>">

                                          <button type="submit" class="btn btn-primary btn-user btn-block" name="edit_ejes">
                                              Registrar Datos
                                          </button>
                                          <hr>
                                        </form>

                                        <?php 
                                          $Crear = new ControladorEjes();
                                          $Crear -> ctrEditarEjes();

                                        ?>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="modal fade" id="EliminarEjes<?=$value['IdEjes']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-md" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Eliminar Ejes</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                        <form class="user" method="post">
                                          <h3>ESTA SEGURO QUE DESEA ELIMINAR ESTOS DATOS</h3>
                                          

                                          <input type="hidden" name="IdEjes" value="<?=$value['IdEjes']?>">
                                          
                                          <button type="submit" class="btn btn-danger btn-user btn-block" name="eliminar_ejes">
                                              Eliminar Datos
                                          </button>
                                          <hr>
                                        </form>

                                        <?php 
                                          $Crear = new ControladorEjes();
                                          $Crear -> ctrEliminarEjes();

                                        ?>
                                </div>
                              </div>
                            </div>
                          </div>

                      <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
      </div>
    </section>
</div>

  <!-- Modal -->
  <div class="modal fade" id="AddEjes" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Agregar Ejes</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
                <form class="user" method="post">
                  <div class="form-group ">
                      <label>Nombre</label>
                      <input type="text" name="NomEjes" value="" class="form-control form-control-user" placeholder="Nombre">
                  </div>
                  <div class="form-group ">
                      <label>Descipción</label>
                      <textarea name="DescEjes" class="form-control form-control-user" placeholder="Nombre"></textarea>
                  </div>

                  <button type="submit" class="btn btn-primary btn-user btn-block" name="add_ejes">
                      Registrar Datos
                  </button>
                  <hr>
                </form>

                <?php 
                  $Crear = new ControladorEjes();
                  $Crear -> ctrCrearEjes();

                ?>
        </div>
      </div>
    </div>
  </div>