<?php
 
  $valor          = $rutas[0];

  $Idactividades  = Funtion::after("-",$valor);

  $actividades      = Controladortareaactividad::ctrMostrarActividad($Idactividades);

  $tareaactividades = Controladortareaactividad::ctrMostrartarea($Idactividades);



  ?>

<link rel="stylesheet" href="vistas/plugins/upload-dropzone/basic.min.css">
<link rel="stylesheet" href="vistas/plugins/upload-dropzone/dropzone.min.css">

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Tarea actividades</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
             <!--  <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Starter Page</li> -->
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-12">
            <div class="card shadow mb-4">
              <div class="card-header py-3">
                <h4 class="m-1 font-weight-bold text-primary">Actividad</h4>
                <br/>
                  <h6 class="m-1 font-weight-bold text-primary"><?=$actividades['Nomactividades'].' ('.$actividades['descactividades'].')'?></h6> 
                  <a href="" class="btn btn-outline-primary float-right mb-2" data-toggle="modal" data-target="#Addtarea" style="margin-top:-25px">
                    <i class="far fa-plus-square"></i>
                  </a>
              </div>
              <div class="card-body">

                <div class="accordion" id="accordionExample">

                  <?php foreach ($tareaactividades as $key => $value) { ?>

                    <div class="card">
                      <div class="card-header" id="headingOne<?=$value['Idtareaactividades']?>">
                        <h2 class="mb-0">
                          <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne<?=$value['Idtareaactividades']?>" aria-expanded="true" aria-controls="collapseOne<?=$value['Idtareaactividades']?>">
                            <?=$value['DescTarea']?>
                          </button>
                          <a href="" class="btn btn-outline-danger float-right mb-2 ml-2" data-toggle="modal" data-target="#Delevidencia<?=$value['Idtareaactividades']?>" >
                            <i class="far fa-trash-alt"></i>
                          </a>
                          <a href="" class="btn btn-outline-primary float-right mb-2 Addevidencia" data-toggle="modal" data-target="#Addevidencia" data-id="<?=$value['Idtareaactividades']?>">
                            <i class="far fa-plus-square"></i>
                          </a>
                        </h2>
                      </div>

                      <div id="collapseOne<?=$value['Idtareaactividades']?>" class="collapse" aria-labelledby="headingOne<?=$value['Idtareaactividades']?>" data-parent="#accordionExample">
                        
                        <?php 

                       $tareaactividad = Controladortareaactividad::ctrMostrarTareaDetalle($value['Idtareaactividades']);

                        foreach ($tareaactividad as $key => $value) {  ?>

                        <div class="card-body"><?=$value['accion']?></div>
                        <div class="card-footer">
                          <?php 

                          $Evidencia = Controladortareaactividad::ctrMostrarEvidencia($value['id_detalle']);

                          foreach ($Evidencia as $key => $value2) { ?>

                          <img src="<?='vistas/dist/img/evidencias/'.$value['id_detalle'].'/'.$value2['Foto']?>" class="img-thumbnail m-1" style="width: 30%">

                          <?php } ?>

                        </div>

                        <?php } ?>

                      </div>
                    </div>

                    <!-- Eliminar -->
                    <div class="modal fade" id="Delevidencia<?=$value['Idtareaactividades']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-md" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Eliminar Tarea</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                                  <form class="user" method="post">
                                    <h3>ESTA SEGURO QUE DESEA ELIMINAR ESTOS DATOS</h3>
                                    

                                    <input type="hidden" name="Idtareaactividades" value="<?=$value['Idtareaactividades']?>">
                                    
                                    <button type="submit" class="btn btn-danger btn-user btn-block" name="eliminar_tarea">
                                        Eliminar Datos
                                    </button>
                                    <hr>
                                  </form>

                                  <?php 
                                    $Crear = new Controladortareaactividad();
                                    $Crear -> ctrEliminartarea();

                                  ?>
                          </div>
                        </div>
                      </div>
                    </div>

                  <?php } ?>
                </div>

                <div class="table-responsive">
                  <!--dentro de este div va mi contenido
                  <table class="table" id="TableClientes">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Nombre Tarea</th>
                        <th>Descripcion Tarea</th>
                        <th>Estado</th>
                        <th>Indicador</th>
                        <th>Opciones</th> 
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($tareaactividades as $key => $value) { ?>
                        <tr>
                          <td><?=$value['Idtareaactividades']?></td>
                          <td><?=$value['NomTarea']?></td>                           
                          <td><a href="#-<?=$value['Idtareaactividades']?>"><?=$value['DescTarea']?></a></td>                         
                          <td><?=$value['EstadoTarea']?></td>                         
                          <td>
                             <div class="progress bg-primary">
                                <div class="progress-bar" role="progressbar" aria-valuenow="40" aria-valuenin="0" aria-valuesmax="100" style="width:0%">
                                  60% completad0 (success)
                                </div>          
                              </div>
                          </td>
                          <td>
                            <a href="#" class="far fa-edit" data-toggle="modal" data-target="#Edittarea<?=$value['Idtareaactividades']?>"></a>
                            |
                            <a href="" class="far fa-trash-alt btn-danger" data-toggle="modal" data-target="#Eliminartarea<?=$value['Idtareaactividades']?>"></a>
                          </td>
                        </tr>

                          <div class="modal fade" id="Edittarea<?=$value['Idtareaactividades']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Editar tarea</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                        <form class="user" method="post">
                                          <div class="form-group ">
                                              <label>Nombre</label>
                                              <input type="text" name="NomTarea" value="<?=$value['NomTarea']?>" class="form-control form-control-user" placeholder="Nombre">
                                          </div>
                                          <div class="form-group ">
                                              <label>Descipción</label>
                                              <textarea name="DescTarea" class="form-control form-control-user" placeholder="Nombre"><?=$value['DescTarea']?></textarea>
                                          </div>

                                          <div class="form-group ">
                                              <label>Estado</label>
                                              <select name="EstadoTarea" class="form-control form-control-user">
                                                <option value="Completado" <?=Funtion::select('Completado',$value['EstadoTarea'])?>>Completado</option>
                                                <option value="Enproceso" <?=Funtion::select('Enproceso',$value['EstadoTarea'])?>>Enproceso</option>
                                                <option value="Declinado" <?=Funtion::select('Declinado',$value['EstadoTarea'])?>>Declinado</option>
                                              </select>
                                          </div>

                                          <input type="hidden" name="Idtareaactividades" value="<?=$value['Idtareaactividades']?>">

                                          <button type="submit" class="btn btn-primary btn-user btn-block" name="edit_tarea">
                                              Registrar Datos
                                          </button>
                                          <hr>
                                        </form>

                                        <?php 
                                          $Crear = new Controladortareaactividad();
                                          $Crear -> ctrEditartarea();

                                        ?>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="modal fade" id="Eliminartarea<?=$value['Idtareaactividades']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-md" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Eliminar tarea</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                        <form class="user" method="post">
                                          <h3>ESTA SEGURO QUE DESEA ELIMINAR ESTOS DATOS</h3>
                                          

                                          <input type="hidden" name="Idtareaactividades" value="<?=$value['Idtareaactividades']?>">
                                          
                                          <button type="submit" class="btn btn-danger btn-user btn-block" name="eliminar_tarea">
                                              Eliminar Datos
                                          </button>
                                          <hr>
                                        </form>

                                        <?php 
                                          $Crear = new Controladortareaactividad();
                                          $Crear -> ctrEliminartarea();

                                        ?>
                                </div>
                              </div>
                            </div>
                          </div>

                      <?php } ?>
                    </tbody>
                  </table>-->


                </div>
              </div>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
      </div>
    </section>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="Addtarea" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Agregar tarea</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
                <form class="user" method="POST">
                  <div class="form-group ">
                      <label>Nombre</label>
                      <input type="text" name="DescTarea" value="" class="form-control form-control-user" placeholder="Nombre">
                  </div>

                  <input type="hidden" name="Idactividades" value="<?=$Idactividades?>">
                  <button type="submit" class="btn btn-primary btn-user btn-block" name="add_tarea">
                      Registrar Datos
                  </button>
                  <hr>
                </form>

                <?php 
                  $Crear = new Controladortareaactividad();
                  $Crear -> ctrCreartarea();

                ?>
        </div>
      </div>
    </div>
  </div>

   <div class="modal fade" id="Addevidencia" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Agregar evidencia</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
                <form class="user" method="POST" id="RegistrarEvidencia">
                  <div class="form-group ">
                      <label>Accion Realizada</label>
                      <textarea name="DescTarea" class="form-control form-control-user" placeholder="Nombre"></textarea>
                  </div>
                  
                  <div class="form-group ">
                      <label>Estado</label>
                      <select name="EstadoTarea" class="form-control form-control-user">
                        <option value="Completado">Completado</option>
                        <option value="Enproceso">En proceso</option>
                        <option value="Declinado">Declinado</option>
                      </select>
                  </div>

                  <div class="form-group ">
                      <label>Porcentaje</label>
                       <select name="progreso" class="form-control form-control-user">
                        <option value="10">10%</option>
                        <option value="20">20%</option>
                        <option value="30">30%</option>
                        <option value="40">40%</option>
                        <option value="50">50%</option>
                        <option value="60">60%</option>
                        <option value="70">70%</option>
                        <option value="80">80%</option>
                        <option value="90">90%</option>
                        <option value="100">100%</option>
                      </select>
                  </div>
                  <input type="hidden" name="IdEvidencia" id="IdEvidencia" value="">
                  <hr>
                </form>
                <div class="form-group">

                    <label for="Mobile">Evidencias</label>
                    <form action="#" class="dropzone" id="my-awesome-dropzone">
                      <div class="fallback">
                          <input name="file" type="file" multiple />
                      </div>
                    </form>

                </div>

                  <button type="submit" class="btn btn-primary btn-user btn-block" name="add_tarea2" form="RegistrarEvidencia">
                      Registrar Datos
                  </button>

                <?php 
                  $CrearEvidencia = new Controladortareaactividad();
                  $CrearEvidencia -> ctrCrearEvidencia();

                ?>
        </div>
      </div>
    </div>
  </div>

<script src="vistas/plugins/upload-dropzone/dropzone.min.js"></script>
<script src="vistas/plugins/upload-dropzone/dropzone-amd-module.min.js"></script>
<script>
  //https://www.dropzonejs.com/#configuration
  Dropzone.options.myAwesomeDropzone = {
    acceptedFiles: ".jpg,.jpeg,.png,.gif", // The name that will be used to transfer the file
    maxFilesize: 10, // MB
    maxFiles: 20,
    thumbnailMethod: "contain",
    resizeHeight:800,
    addRemoveLinks: true,

    removedfile: function(file) {
        var name = file.name;

        $.ajax({
                  type: 'POST',
                  url: 'ajax/dropzone.php',
                  data: "id="+name,
                  dataType: 'html',
                  success: function(data) {
                           //console.log(data);

                    }
              });

          var _ref;
          return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;

        }

  };

  /*myDropzone.on("complete", function(file) {
    myDropzone.removeFile(file);
  });*/
</script>
