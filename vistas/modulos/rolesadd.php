<?php

    $respuesta = ControladorPermiso::ctrMostrarPermiso(null);

    $ruta               = null;

    @$rutas             = explode("/", $_GET["ruta"]);

    $valor              = $rutas[0];

    $IdRoles            = Funtion::after("-",$valor);


    if (isset($IdRoles)) {

      /////////////////////////////////
      //verificar acceso a este modulo
      /////////////////////////////////
      Funtion::Acceso(18);

      //consultamos los permisos registrado a este rol
      $PermisosActivo   = ControladorPermiso::ctrMostrarPermiso($IdRoles);

      $Permisos = new ControladorPermiso();
      $Permisos -> ctrUpdateRoles();

    }else{

      /////////////////////////////////
      //verificar acceso a este modulo
      /////////////////////////////////
      Funtion::Acceso(17);

      //envio datos para realizar el registro
      $Permisos = new ControladorPermiso();
      $Permisos -> ctrCrearRoles();

    }




 ?>

       <!-- Begin Page Content -->
        <div class="container-fluid">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="home">Dashboard</a></li>
              <li class="breadcrumb-item"><a href="roles">Roles</a></li>
              <li class="breadcrumb-item active" aria-current="page">Registrar Roles</li>
            </ol>
          </nav>
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Registrar Roles</h6>
            </div>
            <div class="card-body">

                <form action="" method="POST">
                  <div class="form-group row">
                      <div class="col-sm-12 mb-3 mb-sm-0">
                        <input type="text" name="nombre" class="form-control form-control-user" placeholder="Nombre" required="" value="<?=@$PermisosActivo[0]['nombre']?>">
                      </div>

                      <div class="col-sm-12 mb-3 mb-sm-0">
                        <hr>
                        <h5>Listado de permisos</h5>
                      </div>

                      <div class="col-sm-12 mb-3 mb-sm-0">
                        <div class="table-responsive">
                          <table class="table table-striped dt-responsive nowrap" style="width:100%">
                              <thead>
                                  <tr>
                                      <th>ID</th>
                                      <th>Nombre</th>
                                      <th>Funcionalidad</th>
                                  </tr>
                              </thead>
                              <tbody>
                                <?php

                                $estado ='';

                                foreach ($respuesta as $key => $value) {

                                  if (isset($IdRoles)) {

                                    $estado = (in_array($value['id_permiso'], array_column($PermisosActivo, 'id_permiso')))? "checked" : "";

                                  }

                                echo
                                  '<tr>
                                    <td>
                                      <div class="custom-control custom-checkbox small">

                                        <input type="checkbox" '.$estado.' class="custom-control-input" name="permiso[]" id="customCheck'.$value['id_permiso'].'" value="'.$value['id_permiso'].'">

                                        <label class="custom-control-label" for="customCheck'.$value['id_permiso'].'">Seleccionar</label>
                                      </div>
                                    </td>
                                    <td>'.$value['nombre'].'</td>
                                    <td><p class="font-italic">'.$value['funcionalidad'].'</p></td>
                                  </tr>';
                                 } ?>
                              </tbody>
                          </table>
                        </div>
                      </div>

                      <div class="col-sm-12 mb-3 mb-sm-0">
                        <hr>
                        <?php  if (isset($IdRoles)) { ?>

                          <input type="hidden" name="id_roles" value="<?=$IdRoles?>">

                          <input type="submit" name="UpdateRoles" value="Actualizar Datos" class="btn btn-primary">

                        <?php }else{ ?>

                          <input type="submit" name="AddRoles" value="Registrar Datos" class="btn btn-primary">

                        <?php } ?>
                    </div>

                  </div>
                </form>

            </div>
          </div>

        </div>
        <!-- /.container-fluid -->