<?php 

  $evidencias = ControladorEvidencias::ctrMostrarEvidencias();

 ?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Evidencias</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
             <!--  <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Starter Page</li> -->
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-12">
            <div class="card shadow mb-4">
              <div class="card-header py-3">
                <!-- <h6 class="m-1 font-weight-bold text-primary">Ejes</h6> -->
                  <a href="" class="btn btn-outline-primary float-right mb-2" data-toggle="modal" data-target="#AddEvidencias">
                    <i class="far fa-plus-square"></i>
                  </a>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <!--dentro de este div va mi contenido-->
                  <table class="table" id="TableClientes">
                    <thead>
                      <tr>
                        <th>ID</th>                        
                        <th>Descripcion Evidencia</th>
                         <th>Foto</th>
                        <th>Indicador</th>
                        <th></th>
                        <th>Opciones</th> 
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($evidencias as $key => $value) { ?>
                        <tr>
                          <td><?=$value['Idevidencia']?></td>                                                  
                          <td><?=$value['Descevidencia']?></td>
                          <td><?=$value['Foto']?></td>
                          <td></td>
                          <td></td>
                          <td>
                            <a href="#" class="far fa-edit" data-toggle="modal" data-target="#EditEvidencias<?=$value['Idevidencia']?>"></a>|
                            |
                            <a href="" class="far fa-trash-alt btn-danger" data-toggle="modal" data-target="#EliminarEvidencias<?=$value['Idevidencia']?>"></a>
                          </td>
                        </tr>

                          <form enctype="multipart/form-data" id="Foto" action="uploader.php" method="POST">
                          <input type="hidden" name="MAX_FILE_SIZE" value="200000" />
                          <input name="uploadedfile" type="file" />
                          <input type="submit" value="Subir archivo" />
                          </form>

                          <div class="modal fade" id="EditEvidencias<?=$value['Idevidencia']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Editar Evidencias</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                        <form class="user" method="post">                                         
                                          <div class="form-group ">
                                              <label>Descipción</label>
                                              <textarea name="Descevidencia" class="form-control form-control-user" placeholder="Nombre"><?=$value['Descevidencia']?></textarea>
                                          </div>
                                           <div class="form-group ">
                                              <label>Foto</label>
                                              <input type="text" name="Foto" value="<?=$value['Foto']?>" class="form-control form-control-user" placeholder="Nombre">
                                          </div>

                                          <input type="hidden" name="Idevidencia" value="<?=$value['Idevidencia']?>">

                                          <button type="submit" class="btn btn-primary btn-user btn-block" name="edit_Evidencias">
                                              Registrar Datos
                                          </button>
                                          <hr>
                                        </form>

                                        <?php 
                                          $Crear = new ControladorEvidencias();
                                          $Crear -> ctrEditarEvidencias();

                                        ?>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="modal fade" id="EliminarEvidencias<?=$value['Idevidencia']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-md" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Eliminar Ejes</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                        <form class="user" method="post">
                                          <h3>ESTA SEGURO QUE DESEA ELIMINAR ESTOS DATOS</h3>
                                          

                                          <input type="hidden" name="Idevidencia" value="<?=$value['Idevidencia']?>">
                                          
                                          <button type="submit" class="btn btn-danger btn-user btn-block" name="eliminar_Evidencias">
                                              Eliminar Datos
                                          </button>
                                          <hr>
                                        </form>

                                        <?php 
                                          $Crear = new ControladorEvidencias();
                                          $Crear -> ctrEliminarEvidencias();

                                        ?>
                                </div>
                              </div>
                            </div>
                          </div>

                      <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
      </div>
    </section>
</div>

  <!-- Modal -->
  <div class="modal fade" id="AddEvidencias" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Agregar Evidencia</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
                <form class="user" method="post">
                  <div class="form-group ">
                      <label>Descipción</label>
                        <textarea name="Descevidencia" class="form-control form-control-user" placeholder="Nombre"><?=$value['Descevidencia']?></textarea>
                  <div class="form-group ">
                      <label>Foto</label>
                       <input type="text" name="Foto" value="<?=$value['Foto']?>" class="form-control form-control-user" placeholder="Nombre">
                  </div>

                  <button type="submit" class="btn btn-primary btn-user btn-block" name="add_Evidencias">
                      Registrar Datos
                  </button>
                  <hr>
                </form>

                <?php 
                  $Crear = new ControladorEvidencias();
                  $Crear -> ctrCrearEvidencias();

                ?>
        </div>
      </div>
    </div>
  </div>