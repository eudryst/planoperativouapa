<?php 

  $Directores = ControladorDirectores::ctrMostrarDirectores();

 ?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Directores</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
             <!--  <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Starter Page</li> -->
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-12">
            <div class="card shadow mb-4">
              <div class="card-header py-3">
                <!-- <h6 class="m-1 font-weight-bold text-primary">Ejes</h6> -->
                  <a href="" class="btn btn-outline-primary float-right mb-2" data-toggle="modal" data-target="#AddDirectores">
                    <i class="far fa-plus-square"></i>
                  </a>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <!--dentro de este div va mi contenido-->
                  <table class="table" id="TableClientes">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Usuario</th>
                        <th>Correo</th>
                        <th>Departamento</th>
                        <th>Estado</th> 
                        <th></th>
                        <th>Opciones</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($Directores as $key => $value) { ?>
                        <tr>
                          <td><?=$value['IdDirector']?></td>                         
                          <td><?=$value['NomDirector']?></a></td>                         
                          <td><?=$value['Idusuario']?></td>
                          <td><?=$value['Correo']?></td>
                          <td><a href="departamento-<?=$value['IdDirector']?>"><?=$value['NomDepartamento']?></a></td>  
                          <td><?=$value['Estado']?></td>  
                          <td></td>                        
                          <td>
                             <a href="#" class="far fa-edit" data-toggle="modal" data-target="#EditDirectores<?=$value['IdDirector']?>"></a>
                            |
                            <a href="" class="far fa-trash-alt btn-danger" data-toggle="modal" data-target="#EliminarDirectores<?=$value['IdDirector']?>"></a>
                          </td>
                        </tr>

                          <div class="modal fade" id="EditDirectores<?=$value['IdDirector']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Editar Director</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                        <form class="user" method="post">
                                          <div class="form-group ">
                                              <label>Nombre</label>
                                              <input type="text" name="NomDirector" value="<?=$value['NomDirector']?>" class="form-control form-control-user" placeholder="Nombre">
                                          </div>
                                           <div class="form-group ">
                                              <label>ID Usuario</label>
                                              <input type="text" name="Idusuario" value="<?=$value['Idusuario']?>" class="form-control form-control-user" placeholder="Nombre">
                                          </div>                                         
                                           <form class="user" method="post">
                                          <div class="form-group ">
                                              <label>Correo</label>
                                              <input type="text" name="Correo" value="<?=$value['Correo']?>" class="form-control form-control-user" placeholder="Nombre">
                                          </div>
                                          <div class="form-group ">
                                              <label>Departamento</label>
                                               <select name="NomDepartamento" class="form-control form-control-user">                         
                                                <option value="CFG" <?=Funtion::select('CFG',$value['NomDepartamento'])?>>CFG</option>
                                                <option value="Comisión Revisora" <?=Funtion::select('Comisión Revisora',$value['NomDepartamento'])?>>Comisión Revisora</option>
                                                <option value="DEA" <?=Funtion::select('DEA',$value['NomDepartamento'])?>>DEA</option>
                                                <option value="Vicerrectoría Académica" <?=Funtion::select('Vicerrectoría Académica',$value['NomDepartamento'])?>>Vicerrectoría Académica</option>
                                                <option value="CUDE" <?=Funtion::select('CUDE',$value['NomDepartamento'])?>>CUDE</option>
                                                <option value="Direcciones de Escuelas" <?=Funtion::select(' Direcciones de Escuelas',$value['NomDepartamento'])?>>  Direcciones de Escuelas</option>
                                                <option value="Gestión Humana" <?=Funtion::select('Gestión Humana',$value['NomDepartamento'])?>>Gestión Humana</option>
                                                <option value="Aseguramiento de la Calidad" <?=Funtion::select('Aseguramiento de la Calidad',$value['NomDepartamento'])?>>Aseguramiento de la Calidad</option>
                                                <option value="CINGEP" <?=Funtion::select('CINGEP',$value['NomDepartamento'])?>>CINGEP</option>
                                                <option value="Investigación Científica" <?=Funtion::select('Investigación Científica',$value['NomDepartamento'])?>>Investigación Científica</option>
                                              </select>
                                              <!-- <textarea name="NomDepartamento" class="form-control form-control-user" placeholder="Nombre"><?=$value['NomDepartamento']?></textarea> -->
                                          </div>
                                           <div class="form-group ">
                                              <label>Estado</label>
                                              <select name="Estado" class="form-control form-control-user">
                                                <option value="Activo" <?=Funtion::select('Activo',$value['Estado'])?>>Activo</option>
                                                <option value="Inactivo" <?=Funtion::select('Inactivo',$value['Estado'])?>>Inactivo</option>
                                              </select>
                                          </div>

                                          <input type="hidden" name="IdDirector" value="<?=$value['IdDirector']?>">

                                          <button type="submit" class="btn btn-primary btn-user btn-block" name="edit_directores">
                                              Registrar Datos
                                          </button>
                                          <hr>
                                        </form>

                                        <?php 
                                          $Crear = new ControladorDirectores();
                                          $Crear -> ctrEditarDirectores();


                                        ?>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="modal fade" id="EliminarDirectores<?=$value['IdDirector']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-md" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Eliminar Directores</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                        <form class="user" method="post">
                                          <h3>ESTA SEGURO QUE DESEA ELIMINAR ESTOS DATOS</h3>
                                          

                                          <input type="hidden" name="IdDirector" value="<?=$value['IdDirector']?>">
                                          
                                          <button type="submit" class="btn btn-danger btn-user btn-block" name="eliminar_director">
                                              Eliminar Datos
                                          </button>
                                          <hr>
                                        </form>

                                        <?php 
                                          $Crear = new ControladorDirectores();
                                          $Crear -> ctrEliminarDirector();

                                        ?>
                                </div>
                              </div>
                            </div>
                          </div>

                      <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
      </div>
    </section>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="AddDirectores" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Agregar Director</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
                <form class="user" method="post">
                  <div class="form-group ">
                      <label>Nombre</label>
                      <input type="text" name="NomDirector" value="" class="form-control form-control-user" placeholder="Nombre">
                  </div>
                  <div class="form-group ">
                      <label>ID Usuario</label>
                      <textarea name="Idusuario" class="form-control form-control-user" placeholder="Nombre"></textarea>
                  </div>
                  <div class="form-group ">
                      <label>Correo</label>
                      <input type="text" name="Correo" value="" class="form-control form-control-user" placeholder="Nombre">
                  </div>
                  <div class="form-group ">
                      <label>Departamento</label>
                        <select name="NomDepartamento" class="form-control form-control-user">                         
                          <option value="CFG" <?=Funtion::select('CFG',$value['NomDepartamento'])?>>CFG</option>
                          <option value="Comisión Revisora" <?=Funtion::select('Comisión Revisora',$value['NomDepartamento'])?>>Comisión Revisora</option>
                          <option value="DEA" <?=Funtion::select('DEA',$value['NomDepartamento'])?>>DEA</option>
                          <option value="Vicerrectoría Académica" <?=Funtion::select('Vicerrectoría Académica',$value['NomDepartamento'])?>>Vicerrectoría Académica</option>
                          <option value="CUDE" <?=Funtion::select('CUDE',$value['NomDepartamento'])?>>CUDE</option>
                          <option value="Direcciones de Escuelas" <?=Funtion::select(' Direcciones de Escuelas',$value['NomDepartamento'])?>>  Direcciones de Escuelas</option>
                          <option value="Gestión Humana" <?=Funtion::select('Gestión Humana',$value['NomDepartamento'])?>>Gestión Humana</option>
                          <option value="Aseguramiento de la Calidad" <?=Funtion::select('Aseguramiento de la Calidad',$value['NomDepartamento'])?>>Aseguramiento de la Calidad</option>
                          <option value="CINGEP" <?=Funtion::select('CINGEP',$value['NomDepartamento'])?>>CINGEP</option>
                          <option value="Investigación Científica" <?=Funtion::select('Investigación Científica',$value['NomDepartamento'])?>>Investigación Científica</option>
                        </select>
                    <!--   <textarea name="NomDepartamento" class="form-control form-control-user" placeholder="Nombre"></textarea> -->
                  </div>
                  <div class="form-group ">
                      <label>Estado</label>
                      <select name="Estado" class="form-control form-control-user">
                        <option value="Activo">Activo</option>
                        <option value="Inactivo">Inactivo</option>
                      </select>
                  </div>
                 

                  <button type="submit" class="btn btn-primary btn-user btn-block" name="add_director">
                      Registrar Datos
                  </button>
                  <hr>
                </form>

                <?php 
                  $Crear = new ControladorDirectores();
                  $Crear -> ctrCrearDirectores();

                ?>
        </div>
      </div>
    </div>
  </div>
</div>
