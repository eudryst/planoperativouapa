<?php
    $Bitacora = ControladorBitacora::ctrMostrarBitacora($_SESSION["id_empresa"]);

    /////////////////////////////////
    //verificar acceso a este modulo
    /////////////////////////////////
    Funtion::Acceso(22);
 ?>

       <!-- Begin Page Content -->
        <div class="container-fluid">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="home">Dashboard</a></li>
              <li class="breadcrumb-item active" aria-current="page">Bitacora</li>
            </ol>
          </nav>
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Bitacora</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">

                <table id="example" class="table table-striped dt-responsive nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Usuario</th>
                            <th>Fecha</th>
                            <th>Acción</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php
                      $id = 1;
                       foreach ($Bitacora as $key => $value) {

                        echo
                        '<tr>
                          <td>'.$id.'</td>
                          <td>'.$value['usuario'].'</td>
                          <td>'.date('d/m/Y h:m a', strtotime($value['fecha'])).'</td>
                          <td>'.$value['accion'].'</td>
                        </tr>';
                        $id++;
                     } ?>
                    </tbody>
                </table>


              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->
