<?php

    $respuesta = ControladorPermiso::ctrMostrarRoles();
    /////////////////////////////////
    //verificar acceso a este modulo
    /////////////////////////////////
    Funtion::Acceso(12);
 ?>

       <!-- Begin Page Content -->
        <div class="container-fluid">

          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="home">Dashboard</a></li>
              <li class="breadcrumb-item active" aria-current="page">Roles</li>
            </ol>
          </nav>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Listado de Roles</h6>

              <?php if (in_array(17, $_SESSION["Acceso"])): ?>
                <a href="rolesadd" class="btn btn-outline-primary float-right" style="margin-top: -20px;">
                    <i class="fas fa-id-card-alt"></i> Nuevo
                </a>
              <?php endif ?>

            </div>
            <div class="card-body">
              <div class="table-responsive">

                <table id="example" class="table table-striped dt-responsive nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th><i class="fas fa-sliders-h"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($respuesta as $key => $value) {
                        echo
                        '<tr>
                          <td>'.$value['id_roles'].'</td>
                          <td>'.$value['nombre'].'</td>
                          <td>';

                            if (in_array(18, $_SESSION["Acceso"])):
                              echo '<a href="rolesadd-'.$value['id_roles'].'" class="btn btn-outline-primary">
                                <i class="far fa-edit"></i>
                              </a>';
                            endif;

                            echo '<!--
                            <a href="" class="btn btn-outline-primary">
                              <i class="far fa-trash-alt"></i>
                            </a>
                            -->
                          </td>
                        </tr>';

                       } ?>
                    </tbody>
                </table>

              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->