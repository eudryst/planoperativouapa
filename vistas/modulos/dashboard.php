<?php
 $ejes 					= ControladorEjes::ctrMostrarEjes(); 
 $ObjetivoEstratejico	= Controladorobjetivosestratejicos::ctrMostrarobjetivosestratejicosDashboard();
 $objetivostacticos		= Controladorobjetivostacticos::ctrMostrarobjetivostacticosDashboard();
 $lineasactuacion		= Controladorlineasactuacion::ctrMostrarlineasactuacionDashboard();
 $Metas 				= ControladorMetas::ctrMostrarMetasDashboard();
 $actividades  			= Controladoractividades::ctrMostraractividadesDashboard();
 ?>


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
           <!--  <h1 class="m-0 text-dark">Detalles de registros de los formularios</h1> -->
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
             <!--  <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Starter Page</li> -->
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-12">
            <div class="card shadow mb-4">
              <div class="card-header py-3">
                <h3 class="m-1 font-weight-bold text-primary">DETALLES DEL PLAN OPERATIVO</h3>
                  <!-- <a href="" class="btn btn-outline-primary float-right mb-2" data-toggle="modal" data-target="#AddDirectores">
                    <i class="far fa-plus-square"></i>
                  </a> -->
              </div>
              <div class="card-body">
                <div class="table-responsive">

			    <div id="accordion">
			     <!--  <H4>Detalle</H4> -->
			  <div class="card">
			    <div class="card-header" id="headingOne">
			      <h5 class="mb-0">
			        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
			          Ejes
			        </button>
			      </h5>
			    </div>
			    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
			      <div class="card-body">

				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Nombre</th>
							<th>Descripción</th>
							<th>Estado</th>
						</tr>
					</thead>
					<tbody>
						
				      	<?php foreach ($ejes as $key => $value) { ?>

				      	<tr>
							<td><strong><?=$value['NomEjes'] ?></strong></td>
							<td><?=$value['DescEjes'] ?></td>
							<td>
					          	<div class="progress">
								  <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25% </div>
								</div>
							</td>
						</tr>
				        
				        <?php } ?>

			    </tbody>
			</table>

			        <!-- <div class="progress bg-danger">
			          <div class="progress-bar" role="progressbar" aria-valuenow="40" aria-valuenin="0" aria-valuesmax="100" style="width:40%">
			            40% completad0 (success)
			          </div>          
			        </div>
			        Ejecucion de ejes -->

			      </div>
			    </div>
			  </div>
			  <div class="card">
			    <div class="card-header" id="headingTwo">
			      <h5 class="mb-0">
			        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
			          Objetivos Estrategicos
			        </button>
			      </h5>
			    </div>
			    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
			      <div class="card-body">
			        <table class="table table-bordered">
					<thead>
						<tr>
							<th>Nombre</th>
							<th>Descripción</th>
							<th>Estado</th>
						</tr>
					</thead>
					<tbody>
						
				      	<?php foreach ($ObjetivoEstratejico as $key => $value) { ?>

				      	<tr>
							<td><strong><?=$value['nomobjestratejico'] ?></strong></td>
							<td><?=$value['descobjestratejico'] ?></td>
							<td>
					          	<div class="progress">
								  <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25% </div>
								</div>
							</td>
						</tr>
				        
				        <?php } ?>
 					</tbody>
				</table>

			       
			      </div>
			    </div>
			  </div>
			  <div class="card">
			    <div class="card-header" id="headingThree">
			      <h5 class="mb-0">
			        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
			          Objetivos Tacticos
			        </button>
			      </h5>
			    </div>
			    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
			      <div class="card-body">
			        <table class="table table-bordered">
					<thead>
						<tr>
							<th>Nombre</th>
							<th>Descripción</th>
							<th>Estado</th>
						</tr>
					</thead>
					<tbody>
						
				      	<?php foreach ($objetivostacticos as $key => $value) { ?>

				      	<tr>
							<td><strong><?=$value['Nomobjtactico'] ?></strong></td>
							<td><?=$value['descobjtactico'] ?></td>
							<td>
					          	<div class="progress">
								  <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25% </div>
								</div>
							</td>
						</tr>
				        
				        <?php } ?>
 					</tbody>
				</table>

			       
			      </div>
			    </div>
			  </div>
			  <div class="card">
			    <div class="card-header" id="headingFour">
			      <h5 class="mb-0">
			        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
			          Lineas de Actuacion
			        </button>
			      </h5>
			    </div>
			    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
			        <div class="card-body">
			        <table class="table table-bordered">
					<thead>
						<tr>
							<th>Nombre</th>
							<th>Descripción</th>
							<th>Estado</th>
						</tr>
					</thead>
					<tbody>
						
				      	<?php foreach ($lineasactuacion as $key => $value) { ?>

				      	<tr>
							<td><strong><?=$value['Nomlinactuacion'] ?></strong></td>
							<td><?=$value['DescLinactuacion'] ?></td>
							<td>
					          	<div class="progress">
								  <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25% </div>
								</div>
							</td>
						</tr>
				        
				        <?php } ?>
	 					</tbody>
					</table>

			       
			      </div>
			    </div>
			  </div>
			  <div class="card">
			    <div class="card-header" id="headingFive">
			      <h5 class="mb-0">
			        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
			          Meta
			        </button>
			      </h5>
			    </div>
			    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
			       <div class="card-body">
			        <table class="table table-bordered">
					<thead>
						<tr>
							<th>Nombre</th>
							<th>Descripción</th>
							<th>Estado</th>
						</tr>
					</thead>
					<tbody>
						
				      	<?php foreach ($Metas as $key => $value) { ?>

				      	<tr>
							<td><strong><?=$value['Nommeta'] ?></strong></td>
							<td><?=$value['Descmetas'] ?></td>
							<td>
					          	<div class="progress">
								  <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25% </div>
								</div>
							</td>
						</tr>
				        
				        <?php } ?>
	 				</tbody>
				</table>

			       
			      </div>
			    </div>
			  </div>
			  <div class="card">
			    <div class="card-header" id="headingSix">
			      <h5 class="mb-0">
			        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseThree">
			          Actividades
			        </button>
			      </h5>
			    </div>
			    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
			       <div class="card-body">
			        <table class="table table-bordered">
					<thead>
						<tr>
							<th>Nombre</th>
							<th>Descripción</th>
							<th>Estado</th>
						</tr>
					</thead>
					<tbody>
						
				      	<?php foreach ($actividades as $key => $value) { ?>

				      	<tr>
							<td><strong><?=$value['Nomactividades'] ?></strong></td>
							<td><?=$value['descactividades'] ?></td>
							<td>
					          	<div class="progress">
								  <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25% </div>
								</div>
							</td>
						</tr>
				        
				        <?php } ?>
	 					</tbody>
					</table>

			     </div>  
			      </div>
			    </div>
			  </div>
			</div>
		</div>	        <!-- /.row -->
		</div>
      </div>
    </div>
  </div>
</div>