<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Indicadores</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Starter Page</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-12 col-12">
            
            <!--dentro de este div va mi contenido-->
            <table class="table">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Nombre meta</th>
                  <th>Nombre objetivo tactico</th>
                  <th>Nombre actividades</th>
                  <th>Nombre director</th> 
                  <th>Fecha</th>               
                </tr>
              </thead>
              <tbody>
                 <tr>
                  <td>1</td>
                  <td>Administrativa</td>
                  <td>
                    <a href="" class="btn btn-warning">Editar</a>
                    <a href="" class="btn btn-danger">Eliminar</a>
                  </td>
                </tr>
              </tbody>
            </table>

          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
      </div>
    </section>