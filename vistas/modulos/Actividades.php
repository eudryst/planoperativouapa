<?php
 
  $valor          = $rutas[0];

  $Idactividades  = Funtion::after("-",$valor);

  $actividades    = Controladoractividades::ctrMostraractividades($Idactividades);

 ?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Actividades</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
             <!--  <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Starter Page</li> -->
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-12">
            <div class="card shadow mb-4">
              <div class="card-header py-3">
                <!-- <h6 class="m-1 font-weight-bold text-primary">Ejes</h6> -->
                  <a href="" class="btn btn-outline-primary float-right mb-2" data-toggle="modal" data-target="#Addactividades">
                    <i class="far fa-plus-square"></i>
                  </a>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <!--dentro de este div va mi contenido-->
                  <table class="table" id="TableClientes">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Descripcion</th>
                        <th>Fecha inicio</th>
                        <th>Fecha fin</th>                        
                        <th></th>
                         <th>Opciones</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($actividades as $key => $value) { ?>
                        <tr>
                          <td><?=$value['Idactividades']?></td>
                          <td><a href="tareaactividades-<?=$value['Idactividades']?>"><?=$value['Nomactividades']?></a></td>         
                          <td><?=$value['descactividades']?></td>
                          <td><?=$value['fechainicio']?></td>
                          <td><?=$value['fechafin']?></td>               
                          <td></td>                        
                          <td>
                             <a href="#" class="far fa-edit" data-toggle="modal" data-target="#Editactividades<?=$value['Idactividades']?>"></a>
                            |
                            <a href="" class="far fa-trash-alt btn-danger" data-toggle="modal" data-target="#Eliminaractividades<?=$value['Idactividades']?>"></a>
                          </td>
                        </tr>

                          <div class="modal fade" id="Editactividades<?=$value['Idactividades']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Editar actividad</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                        <form class="user" method="post">
                                          <div class="form-group ">
                                              <label>Nombre</label>
                                              <input type="text" name="Nomactividades" value="<?=$value['Nomactividades']?>" class="form-control form-control-user" placeholder="Nombre">
                                          </div>
                                           <div class="form-group ">
                                              <label>Descripcion</label>
                                              <input type="text" name="descactividades" value="<?=$value['descactividades']?>" class="form-control form-control-user" placeholder="Nombre">
                                          </div>                                         
                                           <form class="user" method="post">
                                          <div class="form-group ">
                                              <label>fechainicio</label>
                                              <input type="date" name="fechainicio" value="<?=$value['fechainicio']?>" class="form-control form-control-user" placeholder="Nombre">
                                          </div>
                                          <div class="form-group ">
                                              <label>Fechafin</label>
                                              <input type="date" name="fechafin" value="<?=$value['fechafin']?>" class="form-control form-control-user" placeholder="Nombre">
                                          </div>
                                          

                                          <input type="hidden" name="Idactividades" value="<?=$value['Idactividades']?>">

                                          <button type="submit" class="btn btn-primary btn-user btn-block" name="edit_actividades">
                                              Registrar Datos
                                          </button>
                                          <hr>
                                        </form>

                                        <?php 
                                          $Crear = new Controladoractividades();
                                          $Crear -> ctrEditaractividades();


                                        ?>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="modal fade" id="Eliminaractividades<?=$value['Idactividades']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-md" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Eliminar actividades</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                        <form class="user" method="post">
                                          <h3>ESTA SEGURO QUE DESEA ELIMINAR ESTOS DATOS</h3>
                                          

                                          <input type="hidden" name="Idactividades" value="<?=$value['Idactividades']?>">
                                          
                                          <button type="submit" class="btn btn-danger btn-user btn-block" name="eliminar_actividades">
                                              Eliminar Datos
                                          </button>
                                          <hr>
                                        </form>

                                        <?php 
                                          $Crear = new Controladoractividades();
                                          $Crear -> ctrEliminaractividades();

                                        ?>
                                </div>
                              </div>
                            </div>
                          </div>

                      <?php } ?>
                    </tbody>
                  </table>

                </div>
              </div>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
      </div>
    </section>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="Addactividades" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Agregar actividad</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
                <form class="user" method="post">
                  <div class="form-group ">
                      <label>Nombre</label>
                      <input type="text" name="Nomactividades" value="" class="form-control form-control-user" placeholder="Nombre">
                  </div>
                  <div class="form-group ">
                      <label>Descripcion</label>
                      <textarea name="descactividades" class="form-control form-control-user" placeholder="Nombre"></textarea>
                  </div>
                  <div class="form-group ">
                      <label>fecha inicio</label>
                      <input type="date" name="fechainicio" value="" class="form-control form-control-user" placeholder="Nombre">
                  </div>
                  <div class="form-group ">
                      <label>fecha fin</label>
                      <input type="date" name="fechafin" class="form-control form-control-user" placeholder="Nombre">
                  </div>
                  
                   <input type="hidden" name="Idmetas" value="<?=$Idactividades?>">

                  <button type="submit" class="btn btn-primary btn-user btn-block" name="add_actividades">
                      Registrar Datos
                  </button>
                  <hr>
                </form>

                <?php 
                  $Crear = new Controladoractividades();
                  $Crear -> ctrCrearactividades();

                ?>
        </div>
      </div>
    </div>
  </div>
