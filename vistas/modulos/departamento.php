<?php 

  $departamento = ControladorDepartamento::ctrMostrarDepartamento();
  

 ?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Departamento</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
             <!--  <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Starter Page</li> -->
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-12">
            <div class="card shadow mb-4">
              <div class="card-header py-3">
                <!-- <h6 class="m-1 font-weight-bold text-primary">Ejes</h6> -->
                  <a href="" class="btn btn-outline-primary float-right mb-2" data-toggle="modal" data-target="#AddDepartamento">
                    <i class="far fa-plus-square"></i>
                  </a>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <!--dentro de este div va mi contenido-->
                  <table class="table" id="TableClientes">
                    <thead>
                      <tr>
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Descripcion</th>
                        <th>Estado</th>  
                        <th></th>
                        <th>Opciones</th>                   
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($departamento as $key => $value) { ?>
                        <tr>
                          <td><?=$value['IdDeparta']?></td>
                          <td><a href="#-<?=$value['IdDeparta']?>"><?=$value['NomDeparta']?></a></td>                          
                          <td><?=$value['DescDeparta']?></td>
                          <td><?=$value['Estado']?></td>
                          <td></td>                          
                          <td>
                            <a href="#" class="far fa-edit" data-toggle="modal" data-target="#EditDepartamento<?=$value['IdDeparta']?>"></a>
                            |
                            <a href="" class="far fa-trash-alt btn-danger" data-toggle="modal" data-target="#EliminarDepartamento<?=$value['IdDeparta']?>"></a>
                          </td>
                        </tr>

                          <div class="modal fade" id="EditDepartamento<?=$value['IdDeparta']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Editar Departamento</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                        <form class="user" method="post">
                                          <div class="form-group ">
                                              <label>Nombre</label>
                                              <input type="text" name="NomDeparta" value="<?=$value['NomDeparta']?>" class="form-control form-control-user" placeholder="Nombre">
                                          </div>
                                          <div class="form-group ">
                                              <label>Descipción</label>
                                              <textarea name="DescDeparta" class="form-control form-control-user" placeholder="Nombre"><?=$value['DescDeparta']?></textarea>
                                          </div>
                                          <div class="form-group ">
                                              <label>Estado</label>
                                              <select name="Estado" class="form-control form-control-user">
                                                <option value="Activo" <?=Funtion::select('Activo',$value['Estado'])?>>Activo</option>
                                                <option value="Inactivo" <?=Funtion::select('Inactivo',$value['Estado'])?>>Inactivo</option>
                                              </select>
                                          </div>

                                          <input type="hidden" name="IdDeparta" value="<?=$value['IdDeparta']?>">

                                          <button type="submit" class="btn btn-primary btn-user btn-block" name="edit_departamentos">
                                              Registrar Datos
                                          </button>
                                          <hr>
                                        </form>

                                        <?php 
                                          $Crear = new ControladorDepartamento();
                                          $Crear -> ctrEditardepartamentos();

                                        ?>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="modal fade" id="EliminarDepartamento<?=$value['IdDeparta']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-md" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Eliminar Departamento</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                        <form class="user" method="post">
                                          <h3>ESTA SEGURO QUE DESEA ELIMINAR ESTOS DATOS</h3>
                                          

                                          <input type="hidden" name="IdDeparta" value="<?=$value['IdDeparta']?>">
                                          
                                          <button type="submit" class="btn btn-danger btn-user btn-block" name="eliminar_departamentos">
                                              Eliminar Datos
                                          </button>
                                          <hr>
                                        </form>

                                        <?php 
                                          $Crear = new ControladorDepartamento();
                                          $Crear -> ctrEliminardepartamentos();

                                        ?>
                                </div>
                              </div>
                            </div>
                          </div>

                      <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
      </div>
    </section>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="AddDepartamento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Agregar Departamento</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
                <form class="user" method="post">
                  <div class="form-group ">
                      <label>Nombre</label>
                      <input type="text" name="NomDeparta" value="" class="form-control form-control-user" placeholder="Nombre">
                  </div>
                  <div class="form-group ">
                      <label>Descipción</label>
                      <textarea name="DescDeparta" class="form-control form-control-user" placeholder="Nombre"></textarea>
                  </div>
                  <div class="form-group ">
                      <label>Estado</label>
                      <select name="Estado" class="form-control form-control-user">
                        <option value="Activo">Activo</option>
                        <option value="Inactivo">Inactivo</option>
                      </select>
                  </div>

                  <button type="submit" class="btn btn-primary btn-user btn-block" name="add_departamento">
                      Registrar Datos
                  </button>
                  <hr>
                </form>

                <?php 
                  $Crear = new ControladorDepartamento();
                  $Crear -> ctrCrearDepartamento();

                ?>
        </div>
      </div>
    </div>
  </div>
