//seleccionar cliente
$("#TableClientes tbody tr").click(function() {

	var id        = $(this).find("td:eq(0)").text();

	var nombre    = $(this).find("td:eq(1)").text();

	document.getElementById("NombreCliente").value=nombre;
	document.getElementById("IdCliente").value=id;

	//lo usamos cuando en una misma pantalle existe el mismo id
	//document.getElementById("NombreCliente2").value=nombre;
	//document.getElementById("IdCliente2").value=id;

	$('#SearchClient').modal('hide');

})

//seleccionar cargo
$("#TableCargo tbody tr").click(function() {

	var id        = $(this).find("td:eq(0)").text();

	var nombre    = $(this).find("td:eq(1)").text();

	document.getElementById("NombreCargo").value=nombre;
	document.getElementById("IdCargo").value=id;

	$('#SearchCargo').modal('hide');

})