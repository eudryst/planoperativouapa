function DelImgBlog(elemento) {

  Swal.fire({
    title: '¿Estás seguro?',
    text: "¡No podrás revertir esto!",
    icon: 'info',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si, elimínalo!'
  }).then((result) => {
    if (result.value) {

        //obtengo el id del archivo a eliminar
        var id = $(elemento).data('value');

        //elimino el archivo
        $.ajax({
            type: 'POST',
            url: 'ajax/dropzone.php',
            data: "IdEdit="+id,
            dataType: 'html',
            success: function(data) {

                   if (data == 'ok') {

                      //eliminamos row de la tabla
                      $(elemento).closest('tr').remove();


                   }

              }
        });

      Swal.fire(
        'Eliminado!',
        'Su archivo ha sido eliminado.',
        'Con éxito'
      )
    }
  })



}



//if (true) {
 // var directorio = '/PYV';
//}else{
 // var directorio = location.host;
//}

//cambiar placeholder de input interes
$("#TipoCuota").change(function(){

  var TipoCuota       = document.getElementById("TipoCuota").value;

  switch (TipoCuota) {
    case '3':
      document.getElementById("interes").placeholder    = "$";
      document.getElementById("InteresValor").innerHTML = "$";
    break;

    default:
      document.getElementById("interes").placeholder    = "%";
      document.getElementById("InteresValor").innerHTML = "%";
      break;
  }

});


function CalcularPrestamoPrint() {
  var Cliente       = document.getElementById("NombreCliente").value;
  var TipoCuota     = document.getElementById("TipoCuota").value;
  var monto         = eval(document.getElementById("monto").value);
  var num_cuotas    = eval(document.getElementById("num_cuotas").value);
  var interes       = eval(document.getElementById("interes").value);
  var fecha_inicio  = document.getElementById("fecha_inicio").value;
  var cuotas        = eval(document.getElementById("cuotas")).value;
  var tasa_mora     = eval(document.getElementById("tasa_mora")).value;
  var comision      = eval(document.getElementById("comision")).value;
  var seguro        = eval(document.getElementById("seguro")).value;
  var observaciones = document.getElementById("observaciones").value;
  var moneda        = document.getElementById("moneda").value;

  //console.log(cuotas);

  var datos         = new FormData();
  datos.append("cliente",       Cliente);
  datos.append("TipoCuota",     TipoCuota);
  datos.append("monto",         monto);
  datos.append("num_cuotas",    num_cuotas);
  datos.append("interes",       interes);
  datos.append("fecha_inicio",  fecha_inicio);
  datos.append("cuotas",        cuotas);
  datos.append("tasa_mora",     tasa_mora);
  datos.append("comision",      comision);
  datos.append("seguro",        seguro);
  datos.append("observaciones", observaciones);
  datos.append("moneda",        moneda);
  datos.append("tipo", "CalcularPrestamo");


  $.ajax({
      type: 'POST',
      url: 'ajax/GenerarPdf.ajax.php',
      data: datos,
      cache: false,
      contentType: false,
      processData: false,
      dataType: "json",
      success: function(data) {

            if (data != '') {

              window.open('vistas/documentos/'+data, '_blank');
              window.focus();
              //location.href= "addprestamos";
            }

        }
  });

}

function ReciboPagoPrint(IdPrestamo,NumPago) {

  var datos         = new FormData();

  datos.append("IdPrestamo",    IdPrestamo);
  datos.append("NumPago",       NumPago);
  datos.append("tipo",          "ReciboPago");


  $.ajax({
      type: 'POST',
      url: 'ajax/GenerarPdf.ajax.php',
      data: datos,
      cache: false,
      contentType: false,
      processData: false,
      dataType: "json",
      success: function(data) {

            if (data != '') {

              window.open('vistas/documentos/'+data, '_blank');
              window.focus();
              //location.href= "CuotaPago-"+IdPrestamo;
            }

        }
  });

}

function ReciboPagoRentaPrint(IdRenta,NumPago) {

  var datos         = new FormData();

  datos.append("IdRenta",       IdRenta);
  datos.append("NumPago",       NumPago);
  datos.append("tipo",          "ReciboPagoRenta");


  $.ajax({
      type: 'POST',
      url: 'ajax/GenerarPdf.ajax.php',
      data: datos,
      cache: false,
      contentType: false,
      processData: false,
      dataType: "json",
      success: function(data) {

            if (data != '') {

              window.open('vistas/documentos/'+data, '_blank');
              window.focus();
              //location.href= "RentaPago-"+IdRenta;
            }

        }
  });

}

function ReciboPrint() {

  var fecha         = document.getElementById("fecha").value;
  var moneda        = document.getElementById("moneda").value;
  var Cliente       = document.getElementById("NombreCliente").value;

  var detalle1      = document.getElementById("detalle1").value;
  var monto1        = eval(document.getElementById("monto1").value);

  var detalle2      = document.getElementById("detalle2").value;
  var monto2        = eval(document.getElementById("monto2").value);

  var detalle3      = document.getElementById("detalle3").value;
  var monto3        = eval(document.getElementById("monto3").value);

  var detalle4      = document.getElementById("detalle4").value;
  var monto4        = eval(document.getElementById("monto4").value);

  var detalle5      = document.getElementById("detalle5").value;
  var monto5        = eval(document.getElementById("monto5").value);

  var total         = eval(document.getElementById("Total").value);

  //console.log(cuotas);

  var datos         = new FormData();
  datos.append("fecha",       fecha);
  datos.append("moneda",      moneda);
  datos.append("Cliente",     Cliente);
  datos.append("detalle1",    detalle1);
  datos.append("monto1",      monto1);
  datos.append("detalle2",    detalle2);
  datos.append("monto2",      monto2);
  datos.append("detalle3",    detalle3);
  datos.append("monto3",      monto3);
  datos.append("detalle4",    detalle4);
  datos.append("monto4",      monto4);
  datos.append("detalle5",    detalle5);
  datos.append("monto5",      monto5);
  datos.append("total",      total);
  datos.append("tipo", "ReciboPrint");


  $.ajax({
      type: 'POST',
      url: 'ajax/GenerarPdf.ajax.php',
      data: datos,
      cache: false,
      contentType: false,
      processData: false,
      dataType: "json",
      success: function(data) {

            if (data != '') {

              window.open('vistas/documentos/'+data, '_blank');
              window.focus();
              //location.href= "addprestamos";
            }

        }
  });

}