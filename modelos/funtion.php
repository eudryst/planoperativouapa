<?php
/**
  *
  */
 class Funtion
 {

    static public function HoursAds($date_time,$time_top,$type){

        $date = new DateTime($date_time);

        $date->modify('+'.$time_top.' '.$type);//type =seconds, hours,day, week

        $date->format('d-m-Y H:i:s');

        return $date = strtotime($date->format('d-m-Y H:i:s'));

    }


    static public function Acceso($permiso){

        if (!in_array($permiso, $_SESSION["Acceso"])):

            echo '<script>window.location = "AccesoDenegado";</script>';

        endif;
    }

    static public function select($dato,$value){

        if ($dato == $value) {

            return 'selected';
        }
    }

    static public function Edad($FechaNacimiento){

        $cumpleanos = new DateTime($FechaNacimiento);

        $hoy = new DateTime();

        $annos = $hoy->diff($cumpleanos);

        return $annos->y;


    }


    static public function EstadoUser($value){

        switch ($value) {
            case 1:
                return '<span class="badge badge-success" style="font-size:13px;">Activo</span>';
                break;
            case 0:
                return '<span class="badge badge-primary" style="font-size:13px;">Inactivo</span>';
                break;
        }
    }



    static public function TotalDeDiasEntreFechas($FechaInicio,$FechaFin){

        //defino fecha 1
        $ano1 = date('Y',strtotime($FechaInicio));
        $mes1 = date('m',strtotime($FechaInicio));
        $dia1 = date('d',strtotime($FechaInicio));

        //defino fecha 2
        $ano2 = date('Y',strtotime($FechaFin));
        $mes2 = date('m',strtotime($FechaFin));
        $dia2 = date('d',strtotime($FechaFin));

        //calculo timestam de las dos fechas
        $timestamp1 = mktime(0,0,0,$mes1,$dia1,$ano1);
        $timestamp2 = mktime(4,12,0,$mes2,$dia2,$ano2);

        //resto a una fecha la otra
        $segundos_diferencia = $timestamp2 -$timestamp1;
        //echo $segundos_diferencia;

        //convierto segundos en días
        $dias_diferencia = $segundos_diferencia / (60 * 60 * 24);

        //obtengo el valor absoulto de los días (quito el posible signo negativo)
        //$dias_diferencia = abs($dias_diferencia);

        //quito los decimales a los días de diferencia
        $dias_diferencia = floor($dias_diferencia);

        return $dias_diferencia;
    }




	////////////////////////mostrar lo que ante de una letra o valor indicado..
    static public function before ($text, $inthat)
    {
        return substr($inthat, 0, strpos($inthat, $text));
    }

    ////////////////////////mostrar lo que sigue despues de una letra o valor indicado..
    static public function after ($text, $inthat)
    {
        if (!is_bool(strpos($inthat, $text)))
        return substr($inthat, strpos($inthat,$text)+strlen($text));
    }

    ////////////////////////Obtenemos el id..
    static public function ObtenerID($ruta){

        $num = substr_count($ruta, '-');

        switch ($num) {
            case 1:
                    return $id_pro = Funtion::after('-',$ruta);
                break;
            case 2:
                    $id_pro = Funtion::after('-',$ruta);
                    return $id_pro = Funtion::after('-',$id_pro);
                break;
            case 3:
                    $id_pro = Funtion::after('-',$ruta);
                    $id_pro = Funtion::after('-',$id_pro);
                    return $id_pro = Funtion::after('-',$id_pro);
                break;
            case 4:
                    $id_pro = Funtion::after('-',$ruta);
                    $id_pro = Funtion::after('-',$id_pro);
                    $id_pro = Funtion::after('-',$id_pro);
                    return $id_pro = Funtion::after('-',$id_pro);
                break;
            default:
                return $id_pro = Funtion::after('-',$ruta);
                break;
        }

    }

    //////////////////////////////////Limpiar string
    static public function ClearString($text){

       $text = str_replace('-', '', $text);


       return $text;
    }


 } ?>