<?php

require_once "conexion.php";

class ModeloMetas{

	/*=============================================
	CREAR Ejes
	=============================================*/

	static public function mdlRegistroMetas($tabla, $datos){

		$pdo = Conexion::conectar();

		$stmt = $pdo->prepare("

			INSERT INTO $tabla ( Idlinactuacion, Nommeta, Descmetas)
			VALUES
				(
					:Idlinactuacion, :Nommeta, :Descmetas
				)
			");

		$stmt->bindParam(":Idlinactuacion", 		$datos['Idlinactuacion'], PDO::PARAM_INT);
		$stmt->bindParam(":Nommeta", 				$datos['Nommeta'], PDO::PARAM_STR);
		$stmt->bindParam(":Descmetas", 				$datos['Descmetas'], PDO::PARAM_STR);
		

		if($stmt->execute()){

			//return $lastId = $pdo->lastInsertId();
			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	Editar Ejes
	=============================================*/

	static public function mdlEditarMetas($tabla, $datos){

		$pdo = Conexion::conectar();

		$stmt = $pdo->prepare("UPDATE $tabla SET Nommeta = :Nommeta, Descmetas = :Descmetas WHERE Idmeta = :Idmeta ");

		$stmt->bindParam(":Nommeta", 		$datos['Nommeta'], PDO::PARAM_STR);
		$stmt->bindParam(":Descmetas", 		$datos['Descmetas'], PDO::PARAM_STR);
		$stmt->bindParam(":Idmeta", 		$datos['Idmeta'], PDO::PARAM_INT);

		if($stmt->execute()){

			//return $lastId = $pdo->lastInsertId();
			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	Elimina Ejes
	=============================================*/

	static public function mdlEliminarMetas($tabla, $datos){

		$pdo = Conexion::conectar();

		$stmt = $pdo->prepare("DELETE FROM $tabla WHERE Idmeta = :Idmeta ");

		$stmt->bindParam(":Idmeta", 		$datos['Idmeta'], PDO::PARAM_INT);

		if($stmt->execute()){

			//return $lastId = $pdo->lastInsertId();
			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	MOSTRAR
	=============================================*/

	static public function mdlMostrarMetas($tabla, $Idlinasctuacion){

		$stmt = Conexion::conectar()->prepare(" SELECT * FROM $tabla WHERE Idlinactuacion = :Idlinactuacion ");

		$stmt->bindParam(":Idlinactuacion", 					$Idlinasctuacion, PDO::PARAM_INT);

		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt -> close();

		$stmt = null;

	}

	static public function mdlMostrarMetasDashboard(){
		
		$stmt = Conexion::conectar()->prepare("
		SELECT
			lineas_actuacion.Nomlinactuacion,
			metas.Nommeta,
			metas.Descmetas 
		FROM
			lineas_actuacion
			INNER JOIN metas ON metas.Idlinactuacion = lineas_actuacion.Idlinactuacion
			 ");

		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt -> close();

		$stmt = null;
	}
}
