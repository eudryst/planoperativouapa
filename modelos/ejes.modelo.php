<?php

require_once "conexion.php";

class ModeloEjes{

	/*=============================================
	CREAR Ejes
	=============================================*/

	static public function mdlRegistroEjes($tabla, $datos){

		$pdo = Conexion::conectar();

		$stmt = $pdo->prepare("

			INSERT INTO $tabla ( NomEjes, DescEjes)
			VALUES
				(
					:NomEjes, :DescEjes
				)
			");

		$stmt->bindParam(":NomEjes", 		$datos['NomEjes'], PDO::PARAM_STR);
		$stmt->bindParam(":DescEjes", 		$datos['DescEjes'], PDO::PARAM_STR);
		

		if($stmt->execute()){

			//return $lastId = $pdo->lastInsertId();
			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	Editar Ejes
	=============================================*/

	static public function mdlEditarEjes($tabla, $datos){

		$pdo = Conexion::conectar();

		$stmt = $pdo->prepare("UPDATE $tabla SET NomEjes = :NomEjes, DescEjes = :DescEjes WHERE IdEjes = :IdEjes ");

		$stmt->bindParam(":NomEjes", 		$datos['NomEjes'], PDO::PARAM_STR);
		$stmt->bindParam(":DescEjes", 		$datos['DescEjes'], PDO::PARAM_STR);
		$stmt->bindParam(":IdEjes", 		$datos['IdEjes'], PDO::PARAM_INT);

		if($stmt->execute()){

			//return $lastId = $pdo->lastInsertId();
			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	Elimina Ejes
	=============================================*/

	static public function mdlEliminarEjes($tabla, $datos){

		$pdo = Conexion::conectar();

		$stmt = $pdo->prepare("DELETE FROM $tabla WHERE IdEjes = :IdEjes ");

		$stmt->bindParam(":IdEjes", 		$datos['IdEjes'], PDO::PARAM_INT);

		if($stmt->execute()){

			//return $lastId = $pdo->lastInsertId();
			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	MOSTRAR
	=============================================*/

	static public function mdlMostrarEjes($tabla){

		$stmt = Conexion::conectar()->prepare(" SELECT * FROM $tabla ");

		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt -> close();

		$stmt = null;

	}
}
