<?php

require_once "conexion.php";

class ModeloPermiso{

	/*=============================================
	CREAR Roles
	=============================================*/

	static public function mdlRegistroRoles($tabla, $datos){

		$pdo = Conexion::conectar();

		$stmt = $pdo->prepare("INSERT INTO $tabla (`nombre`) VALUES (:nombre)");

		$stmt->bindParam(":nombre", $datos['nombre'], PDO::PARAM_STR);

		if($stmt->execute()){

			return $lastId = $pdo->lastInsertId();

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	static public function mdlRegistroRolesPermisos($tabla, $datos){

		$pdo = Conexion::conectar();

		$stmt = $pdo->prepare("INSERT INTO $tabla (`id_roles`, `id_permiso`) VALUES (:id_roles,:id_permiso)");

		$stmt->bindParam(":id_roles", $datos['id_roles'], PDO::PARAM_INT);
		$stmt->bindParam(":id_permiso", $datos['id_permiso'], PDO::PARAM_INT);

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}
	/*=============================================
	MOSTRAR Permiso por usuario
	=============================================*/

	static public function mdlMostrarPermisoUser($valor){

		$stmt = Conexion::conectar()->prepare("SELECT
				permiso.id_permiso
				FROM
				usuario
				INNER JOIN empleado ON usuario.id_empleado = empleado.id_empleado ,
				roles
				INNER JOIN roles_permiso ON roles_permiso.id_roles = roles.id_roles
				INNER JOIN permiso ON roles_permiso.id_permiso = permiso.id_permiso
				WHERE
				roles.id_roles = empleado.id_roles AND usuario.id_usuario = :id_usuario");

		$stmt -> bindParam(":id_usuario", $valor, PDO::PARAM_INT);

		$stmt -> execute();

		return $stmt -> fetchAll();


		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	MOSTRAR Roles
	=============================================*/

	static public function mdlMostrarRoles(){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM roles ");

		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	MOSTRAR Permisos
	=============================================*/

	static public function mdlMostrarPermiso($value){

		if (isset($value)) {

			$stmt = Conexion::conectar()->prepare("SELECT
					roles.nombre,
					roles_permiso.id_roles,
					roles_permiso.id_permiso
					FROM
					roles
					INNER JOIN roles_permiso ON roles_permiso.id_roles = roles.id_roles
					WHERE
					roles.id_roles = :id_roles ");

			$stmt -> bindParam(":id_roles", $value, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetchAll();

		}else{

			$stmt = Conexion::conectar()->prepare("SELECT * FROM `permiso` ");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}

			$stmt -> close();

			$stmt = null;


	}

	/*=============================================
	EDITAR Permiso
	=============================================*/

	static public function mdlEditarPermiso($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET descripcion = :descripcion, funcionalidad = :funcionalidad WHERE id_permiso = :id_permiso");

		$stmt -> bindParam(":descripcion", $datos["descripcion"], PDO::PARAM_STR);
		$stmt -> bindParam(":funcionalidad", $datos["funcionalidad"], PDO::PARAM_STR);
		$stmt -> bindParam(":id_permiso", $datos["id_permiso"], PDO::PARAM_INT);

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	EDITAR Roles
	=============================================*/

	static public function mdlUpdateRoles($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET nombre = :nombre WHERE id_roles = :id_roles");

		$stmt -> bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);
		$stmt -> bindParam(":id_roles", $datos["id_roles"], PDO::PARAM_INT);

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}
	/*=============================================
	BORRAR Permiso
	=============================================*/

	static public function mdlBorrarPermiso($tabla, $item, $value){

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE $item = :$item");

		$stmt -> bindParam(":".$item, $value, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt -> close();

		$stmt = null;

	}

}