<?php

require_once "conexion.php";

class ModeloUsuarios{

	/*=============================================
	MOSTRAR USUARIOS
	=============================================*/


	static public function mdlMostrarUsuariosRegistro($tabla, $item, $valor){

			$stmt = Conexion::conectar()->prepare("SELECT COUNT(*) AS total FROM $tabla WHERE $item = :$item");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch();

		}

	static public function mdlMostrarUsuarios($tabla, $item, $valor){

		if($item != null){

			$stmt = Conexion::conectar()->prepare("
				SELECT
					usuario.Idusuario,
					usuario.Nombre,
					usuario.email,
					usuario.Usuario,
					usuario.password,
					usuario.estado,
					roles.Rol
				FROM
					usuario
					INNER JOIN roles ON usuario.Idrol = roles.Idrol
					WHERE
					usuario.Usuario = :$item");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch();

		}else{

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}


		$stmt -> close();

		$stmt = null;

	}

	static public function mdlMostrarUsuariosProfile($valor){


		$stmt = Conexion::conectar()->prepare("SELECT
		usuario.usuario,
		empleado.fecha_inicio,
		empleado.fecha_fin,
		persona.id_persona,
		persona.cedula,
		persona.apellido,
		persona.nombre,
		persona.fecha_nacimiento,
		persona.telefono,
		persona.email,
		persona.imagen,
		persona.sexo,
		persona.direccion,
		roles.nombre AS cargo
		FROM
		usuario
		INNER JOIN empleado ON usuario.id_empleado = empleado.id_empleado
		INNER JOIN persona ON empleado.id_persona = persona.id_persona
		INNER JOIN roles ON empleado.id_roles = roles.id_roles
		WHERE
		usuario.id_usuario = :id_usuario ");

		$stmt -> bindParam(":id_usuario", $valor, PDO::PARAM_INT);

		$stmt -> execute();

		return $stmt -> fetch();


		$stmt -> close();

		$stmt = null;

	}

	static public function mdlMostrarUsuariosEmp($valor){

		$stmt = Conexion::conectar()->prepare("
				SELECT
					usuario.id_usuario,
					usuario.usuario,
					usuario.contrasena,
					empleado.estado,
					roles.nombre AS rol,
					persona.nombre,
					persona.apellido,
					persona.email,
					persona.cedula,
					persona.fecha,
					roles.id_roles
				FROM
					usuario
					INNER JOIN empleado ON usuario.id_empleado = empleado.id_empleado
					INNER JOIN roles ON empleado.id_roles = roles.id_roles
					INNER JOIN persona ON empleado.id_persona = persona.id_persona
				WHERE
					persona.id_empresa = :id_empresa ");

		$stmt -> bindParam(":id_empresa", $valor, PDO::PARAM_INT);

		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	PERMISOS USUARIOS
	=============================================*/

	static public function mdlPermisoUsuarios($tabla, $item, $valor){

		if($item != null){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetchAll();

		}else{

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}


		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	REGISTRO DE USUARIO
	=============================================*/

	static public function mdlIngresarUsuario($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(id_empleado, usuario, contrasena) VALUES (:id_empleado, :usuario, :contrasena)");

		$stmt->bindParam(":id_empleado", $datos["id_empleado"], PDO::PARAM_INT);
		$stmt->bindParam(":usuario", $datos["LogUsuario"], PDO::PARAM_STR);
		$stmt->bindParam(":contrasena", $datos["password"], PDO::PARAM_STR);

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt->close();

		$stmt = null;

	}

	/*=============================================
	EDITAR USUARIO
	=============================================*/

	static public function mdlEditarUsuario($datos){

		$stmt = Conexion::conectar()->prepare("
			UPDATE empleado
			INNER JOIN usuario ON usuario.id_empleado = empleado.id_empleado
			SET empleado.id_roles = :IdCargo,
			usuario.contrasena = :contrasena,
			empleado.estado = :estado
			WHERE
				usuario.id_usuario = :id_usuario ");

		$stmt -> bindParam(":IdCargo", 		$datos["id_roles"], PDO::PARAM_INT);
		$stmt -> bindParam(":contrasena", 	$datos["contrasena"], PDO::PARAM_STR);
		$stmt -> bindParam(":estado", 		$datos["estado"], PDO::PARAM_INT);
		$stmt -> bindParam(":id_usuario", 	$datos["id_usuario"], PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	ACTUALIZAR USUARIO
	=============================================*/

	static public function mdlActualizarUsuario($tabla, $item1, $valor1, $item2, $valor2){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET $item1 = :$item1 WHERE $item2 = :$item2");

		$stmt -> bindParam(":".$item1, $valor1, PDO::PARAM_STR);
		$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	BORRAR USUARIO
	=============================================*/

	static public function mdlBorrarUsuario($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id = :id");

		$stmt -> bindParam(":id", $datos, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt -> close();

		$stmt = null;


	}

}