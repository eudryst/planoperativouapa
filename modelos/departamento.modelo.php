<?php

require_once "conexion.php";

class Modelodepartamento{

	/*=============================================
	CREAR Departamento
	=============================================*/

	static public function mdlRegistroDepartamento($tabla, $datos){

		$pdo = Conexion::conectar();

		$stmt = $pdo->prepare("

			INSERT INTO $tabla ( NomDeparta, DescDeparta, Estado)
			VALUES
				(
					:NomDeparta, :DescDeparta, :Estado
				)
			");

		$stmt->bindParam(":NomDeparta", 		$datos['NomDeparta'], PDO::PARAM_STR);
		$stmt->bindParam(":DescDeparta", 		$datos['DescDeparta'], PDO::PARAM_STR);
		$stmt->bindParam(":Estado", 			$datos['Estado'], PDO::PARAM_STR);

		if($stmt->execute()){

			//return $lastId = $pdo->lastInsertId();
			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	Editar Ejes
	=============================================*/

	static public function mdlEditarDepartamento($tabla, $datos){

		$pdo = Conexion::conectar();

		$stmt = $pdo->prepare("UPDATE $tabla SET NomDeparta = :NomDeparta, DescDeparta = :DescDeparta, Estado = :Estado WHERE IdDeparta = :IdDeparta ");

		$stmt->bindParam(":NomDeparta", 		$datos['NomDeparta'], PDO::PARAM_STR);
		$stmt->bindParam(":DescDeparta", 		$datos['DescDeparta'], PDO::PARAM_STR);
		$stmt->bindParam(":Estado", 			$datos['Estado'], PDO::PARAM_STR);
		$stmt->bindParam(":IdDeparta", 			$datos['IdDeparta'], PDO::PARAM_INT);

		if($stmt->execute()){

			//return $lastId = $pdo->lastInsertId();
			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	Elimina Ejes
	=============================================*/

	static public function mdlEliminarDepartamento($tabla, $datos){

		$pdo = Conexion::conectar();

		$stmt = $pdo->prepare("DELETE FROM $tabla WHERE IdDeparta = :IdDeparta ");

		$stmt->bindParam(":IdDeparta", 		$datos['IdDeparta'], PDO::PARAM_INT);

		if($stmt->execute()){

			//return $lastId = $pdo->lastInsertId();
			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	MOSTRAR
	=============================================*/

	static public function mdlMostrarDepartamento($tabla){

		$stmt = Conexion::conectar()->prepare(" SELECT * FROM $tabla ");

		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt -> close();

		$stmt = null;

	}
}
