<?php

require_once "conexion.php";

class Modelolineasactuacion{

	/*=============================================
	CREAR Objetivo
	=============================================*/

	static public function mdlRegistrolineasactuacion($tabla, $datos){

		$pdo = Conexion::conectar();

		$stmt = $pdo->prepare("

			INSERT INTO $tabla ( Idobjtactico, Nomlinactuacion, DescLinactuacion)
			VALUES
				(
					:Idobjtactico, :Nomlinactuacion, :DescLinactuacion
				)
			");

		$stmt->bindParam(":Idobjtactico", 				$datos['Idobjtactico'], PDO::PARAM_STR);
		$stmt->bindParam(":Nomlinactuacion", 			$datos['Nomlinactuacion'], PDO::PARAM_STR);
		$stmt->bindParam(":DescLinactuacion", 			$datos['DescLinactuacion'], PDO::PARAM_STR);
		

		if($stmt->execute()){

			//return $lastId = $pdo->lastInsertId();
			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	Editar Ejes
	=============================================*/

	static public function mdlEditarlineasactuacion($tabla, $datos){

		$pdo = Conexion::conectar();

		$stmt = $pdo->prepare("UPDATE $tabla SET Nomlinactuacion = :Nomlinactuacion, DescLinactuacion = :DescLinactuacion WHERE Idlinactuacion = :Idlinactuacion ");

		$stmt->bindParam(":Nomlinactuacion", 		$datos['Nomlinactuacion'], PDO::PARAM_STR);
		$stmt->bindParam(":DescLinactuacion", 		$datos['DescLinactuacion'], PDO::PARAM_STR);
		$stmt->bindParam(":Idlinactuacion", 		$datos['Idlinactuacion'], PDO::PARAM_INT);

		if($stmt->execute()){

			//return $lastId = $pdo->lastInsertId();
			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	Elimina Ejes
	=============================================*/

	static public function mdlEliminarlineasactuacion($tabla, $datos){

		$pdo = Conexion::conectar();

		$stmt = $pdo->prepare("DELETE FROM $tabla WHERE Idlinactuacion = :Idlinactuacion ");

		$stmt->bindParam(":Idlinactuacion", 		$datos['Idlinactuacion'], PDO::PARAM_INT);

		if($stmt->execute()){

			//return $lastId = $pdo->lastInsertId();
			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	MOSTRAR
	=============================================*/

	static public function mdlMostrarlineasactuacion($tabla, $Idobjtacticos){

		$stmt = Conexion::conectar()->prepare(" SELECT * FROM $tabla WHERE Idobjtactico = :Idobjtactico ");

		$stmt->bindParam(":Idobjtactico", 					$Idobjtacticos, PDO::PARAM_INT);

		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt -> close();

		$stmt = null;

	}

	static public function mdlMostrarlineasactuacionDashboard(){

		$stmt = Conexion::conectar()->prepare("
		SELECT
			obj_tactico.Nomobjtactico,
			lineas_actuacion.Nomlinactuacion,
			lineas_actuacion.DescLinactuacion 
		FROM
			obj_tactico
			INNER JOIN lineas_actuacion ON lineas_actuacion.Idobjtactico = obj_tactico.Idobjtactico
			 ");

		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt -> close();

		$stmt = null;

	}
}
