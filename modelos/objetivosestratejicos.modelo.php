<?php

require_once "conexion.php";

class ModeloObjetivosEstratejicos{

	/*=============================================
	CREAR Objetivo
	=============================================*/

	static public function mdlRegistroObjetivoEstratejico($tabla, $datos){

		$pdo = Conexion::conectar();

		$stmt = $pdo->prepare("

			INSERT INTO $tabla ( IdEjes, nomobjestratejico, descobjestratejico)
			VALUES
				(
					:IdEjes, :nomobjestratejico, :descobjestratejico
				)
			");
		
		$stmt->bindParam(":IdEjes", 					$datos['IdEjes'], PDO::PARAM_INT);
		$stmt->bindParam(":nomobjestratejico", 			$datos['nomobjestratejico'], PDO::PARAM_STR);
		$stmt->bindParam(":descobjestratejico", 		$datos['descobjestratejico'], PDO::PARAM_STR);
		

		if($stmt->execute()){

			//return $lastId = $pdo->lastInsertId();
			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	Editar Ejes
	=============================================*/

	static public function mdlEditarobjetivosestratejicos($tabla, $datos){

		$pdo = Conexion::conectar();

		$stmt = $pdo->prepare("UPDATE $tabla SET nomobjestratejico = :nomobjestratejico, descobjestratejico = :descobjestratejico WHERE Idobjestratejico = :Idobjestratejico ");

		$stmt->bindParam(":nomobjestratejico", 		$datos['nomobjestratejico'], PDO::PARAM_STR);
		$stmt->bindParam(":descobjestratejico", 	$datos['descobjestratejico'], PDO::PARAM_STR);
		$stmt->bindParam(":Idobjestratejico", 		$datos['Idobjestratejico'], PDO::PARAM_INT);

		if($stmt->execute()){

			//return $lastId = $pdo->lastInsertId();
			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	Elimina Ejes
	=============================================*/

	static public function mdlEliminarobjetivosestratejicos($tabla, $datos){

		$pdo = Conexion::conectar();

		$stmt = $pdo->prepare("DELETE FROM $tabla WHERE Idobjestratejico = :Idobjestratejico ");

		$stmt->bindParam(":Idobjestratejico", 		$datos['Idobjestratejico'], PDO::PARAM_INT);

		if($stmt->execute()){

			//return $lastId = $pdo->lastInsertId();
			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	MOSTRAR
	=============================================*/

	static public function mdlMostrarobjetivosestratejicos($tabla, $IdEje){

		$stmt = Conexion::conectar()->prepare(" SELECT * FROM $tabla WHERE IdEjes = :IdEjes ");

		$stmt->bindParam(":IdEjes", 		$IdEje, PDO::PARAM_INT);

		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt -> close();

		$stmt = null;

	}

	static public function mdlMostrarobjetivosestratejicosDashboard(){

		$stmt = Conexion::conectar()->prepare("
		SELECT
			ejes.NomEjes,
			obj_estratejicos.nomobjestratejico,
			obj_estratejicos.descobjestratejico 
		FROM
			ejes
			INNER JOIN obj_estratejicos ON obj_estratejicos.IdEjes = ejes.IdEjes
			 ");

		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt -> close();

		$stmt = null;

	}
}
