<?php

require_once "conexion.php";

class ModeloReportes{

	/*=============================================
	MOSTRAR Reportes
	=============================================*/

	static public function mdlDetalleMetas($datos){

		$stmt = Conexion::conectar()->prepare("SELECT
				metas.Idmeta,
				metas.Nommeta,				
				metas.Descmetas
				FROM
				lineas_actuacion
				INNER JOIN metas ON metas.Idlinactuacion = lineas_actuacion.Idlinactuacion
				WHERE
				metas.id_empresa = :id_empresa
				ORDER BY
				metas.Descmetas ASC ");

		$stmt -> bindParam(":id_empresa", $datos['id_empresa'], PDO::PARAM_INT);

		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt -> close();

		$stmt = null;

	}


}