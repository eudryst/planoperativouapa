<?php

require_once "conexion.php";

class ModeloBitacora{

	/*=============================================
	CREAR Persona
	=============================================*/

	static public function mdlRegistroBitacora($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (id_usuario, accion) VALUES (:id_usuario, :accion)");

		$stmt->bindParam(":id_usuario", $datos['id_usuario'], PDO::PARAM_INT);
		$stmt->bindParam(":accion", 	$datos['accion'], PDO::PARAM_STR);

		if($stmt->execute()){

			return "bien";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	MOSTRAR Cuota
	=============================================*/

	static public function mdlMostrarBitacora($tabla, $valor){

		$stmt = Conexion::conectar()->prepare("SELECT
		bitacora.id_bitacora,
		bitacora.id_empresa,
		bitacora.id_usuario,
		bitacora.fecha,
		bitacora.accion,
		usuario.id_usuario,
		usuario.usuario
		FROM
		bitacora
		INNER JOIN usuario ON bitacora.id_usuario = usuario.id_usuario
		WHERE
		bitacora.id_empresa = $valor ORDER BY `fecha` DESC ");

		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt -> close();

		$stmt = null;

	}
}